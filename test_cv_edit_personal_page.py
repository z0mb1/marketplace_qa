from selenium.webdriver.common.by import By
import settings
import urls
import time
import pytest
from pages.cv_edit_personal_page import OtherContactsSet, CVEditPersonalPage
from pages.locators import CVEditFormPersonal, Jobs
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


jobs_url = settings.DOMAIN + urls.JOBS
cv_edit_personal_url = settings.DOMAIN + urls.CV_EDIT_PERSONAL
cv_edit_common_url = settings.DOMAIN + urls.CV_EDIT_COMMON


@pytest.fixture(scope="function")
def cv_edit_personal_page(browser, make_login_as_candidate):
    WebDriverWait(browser, 10).until(
        EC.title_contains(Jobs.TITLE_CONTAINS_TEXT)
    )
    browser.get(cv_edit_personal_url)
    cv_edit_personal_page = CVEditPersonalPage(browser, cv_edit_personal_url)
    WebDriverWait(browser, 10).until(
        EC.title_contains(CVEditFormPersonal.TITLE_CONTAINS_TEXT)
    )
    return cv_edit_personal_page
'''
#@pytest.mark.skip
def test_can_add_and_delete_avatar(cv_edit_personal_page):
    cv_edit_personal_page.avatar.clear()
    assert not cv_edit_personal_page.avatar.is_avatar_present(), 'should not be avatar before test start'
    cv_edit_personal_page.avatar.fill_input()
    cv_edit_personal_page.avatar.should_avatar_present()
    cv_edit_personal_page.avatar.delete_avatar()
    assert not cv_edit_personal_page.avatar.is_avatar_present(), 'should not be avatar after delete avatar'

@pytest.mark.skip  # этот тест удалить, без регистрации не имеет смысла
def test_fullname_field_have_registration_value(cv_edit_personal_page):
    """!!!тест должен выполняться для кандидата, не заполнявшего анкету"""
    assert settings.CANDIDATE_FULL_NAME == cv_edit_personal_page.fullname.get_value(), 'full name should be equal registration value'

@pytest.mark.skip  # этот тест удалить, без регистрации не имеет смысла
def test_email_field_have_registration_value(cv_edit_personal_page):
    """!!!тест должен выполняться для кандидата, не заполнявшего анкету"""
    assert settings.CANDIDATE_EMAIL == cv_edit_personal_page.email.get_value(), 'email should be equal registration value'

#@pytest.mark.skip
def test_can_add_and_delete_other_contact_block(cv_edit_personal_page):
    # для теста в анкете не должно быть 4 (максимум) контакта, либо вызвать предварительно метод clear() - это замедлит выполнение тестов
    cv_edit_personal_page.other_contacts.add_other_contact()
    cv_edit_personal_page.other_contacts.should_be_correct_number_of_rows()
    cv_edit_personal_page.other_contacts.delete_other_contact()
    cv_edit_personal_page.other_contacts.should_be_correct_number_of_rows()

#@pytest.mark.skip
def test_can_not_continue_without_name(cv_edit_personal_page):
    if cv_edit_personal_page.email.get_value() == "":
        cv_edit_personal_page.email.fill_input("test@test.com")
    if len(cv_edit_personal_page.phone.get_value()) < 3:
        cv_edit_personal_page.phone.fill_input("9999999999")
    cv_edit_personal_page.fullname.clear_value()
    cv_edit_personal_page.save()
    cv_edit_personal_page.should_show_text(CVEditFormPersonal.MISSING_FULLNAME_MESSAGE)

#@pytest.mark.skip
def test_can_not_continue_without_email(cv_edit_personal_page):
    if cv_edit_personal_page.fullname.get_value() == "":
        cv_edit_personal_page.fullname.fill_input()
    if len(cv_edit_personal_page.phone.get_value()) < 3:
        cv_edit_personal_page.phone.fill_input("9999999999")
    cv_edit_personal_page.email.clear_value()
    cv_edit_personal_page.save()
    cv_edit_personal_page.should_show_text(CVEditFormPersonal.MISSING_EMAIL_MESSAGE)

#@pytest.mark.skip
def test_can_not_continue_without_phone(cv_edit_personal_page):
    if cv_edit_personal_page.fullname.get_value() == "":
        cv_edit_personal_page.fullname.fill_input()
    if cv_edit_personal_page.email.get_value() == "":
        cv_edit_personal_page.email.fill_input("test@test.com")
    cv_edit_personal_page.phone.clear_value()
    cv_edit_personal_page.save()
    cv_edit_personal_page.should_show_text(CVEditFormPersonal.MISSING_PHONE_MESSAGE)
'''
#@pytest.mark.skip
def test_save_data_correct(browser, cv_edit_personal_page):
    print(cv_edit_personal_page.get_values())
    cv_edit_personal_page.clear()
    cv_edit_personal_page.fill_values()
    data = cv_edit_personal_page.get_values()
    print(data)
    cv_edit_personal_page.save()
    cv_edit_personal_page.should_show_success_message()
    cv_edit_personal_page.refresh()
    page_after_refresh = CVEditPersonalPage(browser, cv_edit_personal_url)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "data saves incorrect"

@pytest.mark.skip
def test_save_data_with_blank_optional_fields_correct(browser, cv_edit_personal_page):
    cv_edit_personal_page.clear_optional_fields()
    data = cv_edit_personal_page.get_values()
    print(data)
    cv_edit_personal_page.save()
    cv_edit_personal_page.should_show_success_message()
    cv_edit_personal_page.refresh()
    page_after_refresh = CVEditPersonalPage(browser, cv_edit_personal_page)
    saved_data = page_after_refresh.get_values()
    saved_data['other_contacts'] = {}  # после вызова clear_optional_fields поле other_contacts должно быть пустым
    print(saved_data)
    assert data == saved_data, "blank data saves incorrect"
