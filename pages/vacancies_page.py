from .base_page import BasePage
from .locators import VacanciesPageLocators
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class VacanciesPage(BasePage):

    def click_create_vacancies_btn(self):
        WebDriverWait(self.browser, 10).until(
            EC.element_to_be_clickable((By.XPATH, "//button/*[text()='Создать вакансию']"))
        )
        create_vacancy_btn = self.browser.find_element(*VacanciesPageLocators.CREATE_VACANCY_BTN)
        create_vacancy_btn.click()
