from pages.base_page import BasePageCVEdit
from .form_elements import Input, BaseElement, DropDown, Tab, CheckBox, SelectGroup, SingleSelect, \
    MultiSelectWithAutoComplete, CityWithAutocomplete
from .locators import CVEditFormPersonal, CVEditFormCommon
import random
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import settings


class CVEditCommonPage(BasePageCVEdit):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.cv_name = Input(browser, CVEditFormCommon.CV_NAME_INPUT, 'text', True)
        self.about = Input(browser, CVEditFormCommon.ABOUT_INPUT, 'text')
        self.currency = Tab(browser, CVEditFormCommon.CURRENCY_RUB, CVEditFormCommon.CURRENCY_USD)
        self.period = Tab(browser, CVEditFormCommon.PERIOD_MONTH, CVEditFormCommon.PERIOD_YEAR)
        self.salary_min = Input(browser, CVEditFormCommon.SALARY_MIN_INPUT, 'number', True)
        self.salary_public = Input(browser, CVEditFormCommon.SALARY_PUBLIC_INPUT, 'number', True)
        self.show_salary = CheckBox(browser, CVEditFormCommon.SALARY_SHOW_INPUT, CVEditFormCommon.SALARY_SHOW_CHECKBOX)
        self.city = CityWithAutocomplete(browser, CVEditFormCommon.CITY_INPUT, CVEditFormCommon.CITY_VALUE)
        self.ready_to_relocation = CheckBoxWithExtraFields(browser, CVEditFormCommon.READY_FOR_RELOCATION_INPUT,
                                                           CVEditFormCommon.READY_FOR_RELOCATION_CHECKBOX,
                                                           CVEditFormCommon.RELOCATION_CITIES)
        self.office_work = SingleSelect(browser, CVEditFormCommon.WORK_TYPE_OFFICE)
        self.remote_work = SingleSelect(browser, CVEditFormCommon.WORK_TYPE_REMOTE)
        self.specializations = SelectGroup(browser, CVEditFormCommon.SPECIALIZATION_ITEM, settings.SELECTED_MAX_SPECIALIZATIONS)
        self.management_experience = CheckBoxWithExtraFields(browser, CVEditFormCommon.HAS_MANAGEMENT_EXPERIENCE_INPUT,
                                                             CVEditFormCommon.HAS_MANAGEMENT_EXPERIENCE_CHECKBOX,
                                                             CVEditFormCommon.ABOUT_MANAGEMENT_EXPERIENCE_INPUT,
                                                             CVEditFormCommon.MANAGEMENT_EXPERIENCE_PERIOD)
        self.skill_set = SkillSet(browser)
        self.tools = MultiSelectWithAutoComplete(browser, CVEditFormCommon.TOOLS_INPUT, CVEditFormCommon.TOOLS_ELEMENTS, CVEditFormCommon.TOOLS_ELEMENT, CVEditFormCommon.TOOLS_DELETE_ELEMENT)
        self.languages = ForeignLanguages(browser)

    def get_values(self):
        return {'cv_name': self.cv_name.get_value(),
                'about': self.about.get_value(),
                'currency': self.currency.get_value(),
                'period': self.period.get_value(),
                'salary_min': self.salary_min.get_value(),
                'salary_public': self.salary_public.get_value(),
                'show_salary': self.show_salary.get_value(),
                'city': self.city.get_value(),
                'office_work': self.office_work.get_value(),
                'remote_work': self.remote_work.get_value(),
                'specializations': self.specializations.get_value(),
                'skill_set': self.skill_set.get_value(),
                'tools': self.tools.get_value(),
                'languages': self.languages.get_value(),
                }

    def fill_inputs(self):
        self.cv_name.fill_input()
        self.about.fill_input()
        self.currency.click_unselected_value()
        self.period.click_unselected_value()
        #self.salary_min.fill_input() <--- не работает очистка поля
        #self.salary_public.fill_input() <--- не работает очистка поля
        self.show_salary.click()
        self.city.fill_value()
        # self.ready_to_relocation.click() -  пока не трогать откроется новое поле
        self.office_work.make_selected()  # <-- обязательно должна быть выбрана одна из опций "В офисе" или "Удаленно"
        self.remote_work.click()
        self.specializations.clear_elements()
        for _ in range(settings.SELECTED_MAX_SPECIALIZATIONS):
            self.specializations.choose_element()
        # self.management_experience  -  пока не трогать откроется новое поле
        self.skill_set.clear()
        self.skill_set.add_skill()
        self.skill_set.fill_values()
        self.tools.fill_input()
        self.languages.clear()
        self.languages.add_lang()
        self.languages.add_lang()
        self.languages.fill_values()

    def clear(self):
        self.cv_name.clear_value()
        self.about.clear_value()
        self.salary_min.clear_value()
        self.salary_public.clear_value()
        self.specializations.clear_elements()
        self.skill_set.clear()
        self.tools.clear()
        self.languages.clear()

    def clear_optional_fields(self):
        self.about.clear_value()
        self.skill_set.clear()
        self.skill_set.fill_values()
        self.tools.clear()
        self.languages.clear()


class CheckBoxWithExtraFields(CheckBox):

    def __init__(self, browser,  input_selector, clickable_element_selector, *extra_field_selectors, is_required=False):
        super().__init__(browser, input_selector, clickable_element_selector, is_required)
        self.extra_field_selectors = extra_field_selectors

    def should_show_extra_fields(self):
        for extra_field_selector in self.extra_field_selectors:
            if self.is_checked():
                assert self.is_element_present(*extra_field_selector), 'extra field is not present'
            else:
                assert not self.is_element_present(*extra_field_selector), 'extra field is present'


class Skill:
    def __init__(self, browser, index):
        self.browser = browser
        self.index = index
        self.area = DropDown(browser, CVEditFormCommon.AREA_DROPDOWN(index), CVEditFormCommon.AREA_VALUE(index), index)
        self.technology = DropDown(browser, CVEditFormCommon.TECHNOLOGY_DROPDOWN(index), CVEditFormCommon.TECHNOLOGY_VALUE(index), index)  # если не выбрана area, то elements еще не будут инициализированы
        self.experience = DropDown(browser, CVEditFormCommon.EXPERIENCE_DROPDOWN(index), CVEditFormCommon.EXPERIENCE_VALUE(index), index)
        self.search_job_in_stack = CheckBox(browser, CVEditFormCommon.SEARCH_JOB_IN_STACK_INPUT(index), CVEditFormCommon.SEARCH_JOB_IN_STACK_CHECKBOX(index))

    def make_skillset(self):
        self.area.choose_value()
        self.technology = DropDown(self.browser, CVEditFormCommon.TECHNOLOGY_DROPDOWN(self.index), CVEditFormCommon.TECHNOLOGY_VALUE(self.index), self.index)
        self.technology.choose_value()
        self.experience.choose_value()
        if not self.search_job_in_stack.is_checked():
            self.search_job_in_stack.click()

    def delete(self):
        delete_btn = self.browser.find_element(*CVEditFormCommon.DELETE_SKILL_BTN(self.index))
        delete_btn.click()

    def get_value(self):
        return {'area': self.area.get_value(),
                'technology': self.technology.get_value(),
                'experience': self.experience.get_value(),
                'search_job_in_stack': self.search_job_in_stack.get_value()}


class SkillSet:
    def __init__(self, browser):
        self.browser = browser
        self.skills = self.get_skills()
        self.add_skill_btn = self.browser.find_element(*CVEditFormCommon.ADD_TECHNOLOGY_BTN)
        self.count = self.get_number_of_skill_rows()

    def add_skill(self):
        self.add_skill_btn.click()
        self.skills.append(Skill(self.browser, self.count))
        self.count += 1

    def delete_skill(self, index=None):
        if not index:
            index = self.get_number_of_skill_rows() - 1
        skill = self.skills[index]
        skill.delete()
        self.skills.pop()  # при удалении любого элемента индексы оставшихся сдвигаются, т.е. всегда со страницы удалится элемент с наибольшим индексом
        self.count -= 1

    def get_number_of_skill_rows(self):
        return len(self.browser.find_elements(*CVEditFormCommon.SKILL_ROW))

    def clear(self):
        """если блок остается единственным на сранице, у него нет кнопки Удалить"""
        for _ in range(self.get_number_of_skill_rows() - 1):
            self.delete_skill()

    def fill_values(self):
        for skill in self.skills:
            skill.make_skillset()

    def get_value(self):
        return sorted([skill.get_value() for skill in self.skills], key=lambda x: x['technology'])

    def get_skills(self):
        return [Skill(self.browser, n) for n in range(self.get_number_of_skill_rows())]

    def should_be_correct_number_of_skill_rows(self):
        assert self.count == self.get_number_of_skill_rows(), 'number of skill rows is not correct'


class Language:
    def __init__(self, browser, index):
        self.browser = browser
        self.index = index
        self.name = DropDown(browser, CVEditFormCommon.LANGUAGE_DROPDOWN(index),
                             CVEditFormCommon.LANGUAGE_VALUE(index), index)
        self.level = DropDown(browser, CVEditFormCommon.LEVEL_DROPDOWN(index),
                              CVEditFormCommon.LEVEL_VALUE(index), index)
        self.delete_btn = BaseElement(browser, CVEditFormCommon.DELETE_LANG_BTN(index))

    def set_value(self, type_index='random'):
        self.name.choose_value(type_index)
        self.level.choose_value(type_index)

    def delete(self):
        self.delete_btn.click()

    def get_value(self):
        return {'name': self.name.get_value(),
                'level': self.level.get_value()}


class ForeignLanguages:
    def __init__(self, browser):
        self.browser = browser
        self.languages = self.get_languages()
        self.add_element = BaseElement(browser, CVEditFormCommon.ADD_LANG_BTN)
        self.count = self.get_number_of_elements()

    def add_lang(self):
        self.add_element.click()
        self.languages.append(Language(self.browser, self.count))
        self.count += 1

    def delete_lang(self, index=None):
        if not index:
            index = self.get_number_of_elements() - 1
        lang = self.languages[index]
        lang.delete()
        self.languages.remove(lang)
        self.count -= 1

    def fill_values(self):
        for lang in self.languages:
            lang.set_value()

    def get_value(self):
        return sorted([lang.get_value() for lang in self.get_languages()], key=lambda x: x['name'])

    def get_number_of_elements(self):
        return len(self.browser.find_elements(*CVEditFormCommon.LANG_ROW))

    def get_languages(self):
        return [Language(self.browser, n) for n in range(self.get_number_of_elements())]

    def should_be_correct_number_of_elements(self):
        assert self.count == self.get_number_of_elements(), f"number of other contact rows {self.count} is not correct {self.get_number_of_elements()}"

    def clear(self):
        for _ in range(self.get_number_of_elements()):
            self.delete_lang()


