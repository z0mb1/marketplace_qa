from pages.base_page import BasePageCVEdit
from .form_elements import SelectGroup, Input, BaseElement, DropDown, MultiSelectWithAutoComplete, TextInputWithAutocomplete
from .locators import CVEditFormPreferences, CVEditFormExperience, CVEditFormEducation
import random
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import settings


class CVEditEducationPage(BasePageCVEdit):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.institutions = InstitutionSet(browser)

    def fill_inputs(self):
        self.institutions.fill_input()

    def clear(self):
        self.institutions.clear()

    def clear_optional_fields(self):
        self.institutions.clear_optional_fields()

    def get_values(self):
        return {'institutions': self.institutions.get_value()}


class Institution:
    def __init__(self, browser, index):
        self.browser = browser
        self.index = index
        self.institution = TextInputWithAutocomplete(browser, CVEditFormEducation.INSTITUTION_INPUT(index), CVEditFormEducation.INSTITUTION_VALUE(index))
        self.faculty = TextInputWithAutocomplete(browser, CVEditFormEducation.FACULTY_INPUT(index), CVEditFormEducation.FACULTY_VALUE(index))
        self.start_month = DropDown(browser, CVEditFormEducation.START_MONTH_DROPDOWN(index),
                                    CVEditFormEducation.START_MONTH_VALUE(index))
        self.start_year = Input(browser, CVEditFormEducation.START_YEAR_INPUT(index), 'number')
        self.finish_month = DropDown(browser, CVEditFormEducation.FINISH_MONTH_DROPDOWN(index),
                                     CVEditFormEducation.FINISH_MONTH_VALUE(index))
        self.finish_year = Input(browser, CVEditFormEducation.FINISH_YEAR_INPUT(index), 'number')
        # self.to_present

    def fill_values(self):
        self.institution.fill_input()
        self.faculty.fill_input()
        self.start_month.choose_value()
        start_year = 1990 + random.randint(0, 20)
        finish_year = start_year + random.randint(1, 9)
        self.start_year.fill_input(start_year)
        self.finish_month.choose_value()
        self.finish_year.fill_input(finish_year)

    def clear(self):
        self.start_year.clear_value()
        self.finish_year.clear_value()

    def clear_optional_fields(self):
        self.start_year.clear_value()
        self.finish_year.clear_value()

    def delete(self):
        delete_btn = self.browser.find_element(*CVEditFormEducation.DELETE_INSTITUTION_BTN(self.index))
        # delete_btn.click() - по неизвестной причинеиклик отказался работать
        self.browser.execute_script("arguments[0].click();", delete_btn)
        try:
            confirm = self.browser.switch_to.alert
            confirm.accept()
        except:
            pass

    def get_value(self):
        self.start_month = DropDown(self.browser, CVEditFormEducation.START_MONTH_DROPDOWN(self.index),
                                    CVEditFormEducation.START_MONTH_VALUE(self.index))
        self.finish_month = DropDown(self.browser, CVEditFormEducation.FINISH_MONTH_DROPDOWN(self.index),
                                     CVEditFormEducation.FINISH_MONTH_VALUE(self.index))
        return {'institution': self.institution.get_value(),
                'faculty': self.faculty.get_value(),
                'start_month': self.start_month.get_value(),
                'start_year': self.start_year.get_value(),
                'finish_month': self.finish_month.get_value(),
                'finish_year': self.finish_year.get_value(),
                }


class InstitutionSet:
    def __init__(self, browser):
        self.browser = browser
        self.institutions = self.get_institutions()
        self.add_element_btn = BaseElement(browser, CVEditFormEducation.ADD_INSTITUTION_BTN)
        self.count = self.get_number_of_rows()

    def add_institution(self):
        self.add_element_btn.click()
        self.institutions.append(Institution(self.browser, self.count))
        self.count += 1

    def delete_element(self, index=None):
        """удаляет пример кода со страницы, уменьшает счетчик примеров кода, удаляет значение из массива примеров кода"""
        if not index:
            index = self.get_number_of_rows() - 1
        element = self.institutions[index]
        element.delete()
        self.institutions.pop()  # при удалении любого элемента индексы оставшихся сдвигаются, т.е. всегда со страницы удалится элемент с наибольшим индексом
        self.count -= 1

    def get_number_of_rows(self):
        return len(self.browser.find_elements(*CVEditFormEducation.INSTITUTION_ROW))

    def clear(self):
        for _ in range(self.get_number_of_rows()):
            self.delete_element()

    def clear_optional_fields(self):
        for _ in range(self.get_number_of_rows()):
            self.delete_element()

    def fill_values(self):
        for element in self.institutions:
            element.fill_values()

    def get_value(self):
        return sorted([element.get_value() for element in self.institutions], key=lambda x: x['institution'])

    def get_institutions(self):
        return [Institution(self.browser, n) for n in range(self.get_number_of_rows())]

    def should_be_correct_number_of_rows(self):
        assert self.count == self.get_number_of_rows(), f"number of other contact rows {self.count} is not correct {self.get_number_of_rows()}"

    def fill_input(self):
        self.clear()
        self.add_institution()
        self.fill_values()

