from pages.base_page import BasePageCVEdit
from .form_elements import SelectGroup, Input, BaseElement, DropDown, MultiSelectWithAutoComplete
from .locators import CVEditFormPreferences, CVEditFormExperience
import random
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import settings


class CVEditExperiencePage(BasePageCVEdit):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.companies = CompanySet(browser)

    def fill_inputs(self):
        self.companies.fill_input()

    def clear(self):
        self.companies.clear()

    def clear_optional_fields(self):
        self.companies.clear_optional_fields()

    def get_values(self):
        return {'companies': self.companies.get_value()}


class Company:
    def __init__(self, browser, index):
        self.browser = browser
        self.index = index
        self.position = Input(browser, CVEditFormExperience.POSITION_INPUT(index), 'text')
        self.company_name = Input(browser, CVEditFormExperience.COMPANY_NAME_INPUT(index), 'text')
        self.company_link = Input(browser, CVEditFormExperience.COMPANY_LINK_INPUT(index), 'text')
        self.start_month = DropDown(browser, CVEditFormExperience.START_MONTH_DROPDOWN(index), CVEditFormExperience.START_MONTH_VALUE(index))
        self.start_year = Input(browser, CVEditFormExperience.START_YEAR_INPUT(index), 'number')
        self.finish_month = DropDown(browser, CVEditFormExperience.FINISH_MONTH_DROPDOWN(index), CVEditFormExperience.FINISH_MONTH_VALUE(index))
        self.finish_year = Input(browser, CVEditFormExperience.FINISH_YEAR_INPUT(index), 'number')
        #self.is_continious = - не используется в тесте
        self.common_description = Input(browser, CVEditFormExperience.COMMON_DESCRIPTION_TEXTAREA(index), 'text')
        self.technologies = MultiSelectWithAutoComplete(browser,
                                                        CVEditFormExperience.TECHNOLOGIES_INPUT(index),
                                                        CVEditFormExperience.TECHNOLOGIES_ELEMENTS(index),
                                                        # если кто-то это читает, не пугайтесь, это просто селектор внутри lambda, обернутый в другую lambda
                                                        # так надо
                                                        lambda index_el: CVEditFormExperience.TECHNOLOGIES_ELEMENT(index, index_el),
                                                        lambda index_el: CVEditFormExperience.TECHNOLOGIES_DELETE_ELEMENT(index, index_el))
        # раздел с попроектным описанием в данной версии не покрыт тестами

    def fill_values(self):
        self.position.fill_input()
        self.company_name.fill_input()
        self.company_link.fill_input()
        self.start_month.choose_value()
        start_year = 1990 + random.randint(0, 20)
        finish_year = start_year + random.randint(1, 9)
        self.start_year.fill_input(start_year)
        self.finish_month.choose_value()
        self.finish_year.fill_input(finish_year)
        self.common_description.fill_input()
        self.technologies.fill_input()

    def clear(self):
        self.position.clear_value()
        self.company_name.clear_value()
        self.company_link.clear_value()
        self.start_year.clear_value()
        self.finish_year.clear_value()
        self.common_description.clear_value()
        self.technologies.clear()

    def clear_optional_fields(self):
        self.company_link.clear_value()
        self.common_description.clear_value()
        self.technologies.clear()

    def delete(self):
        delete_btn = self.browser.find_element(*CVEditFormExperience.DELETE_COMPANY_BTN(self.index))
        # delete_btn.click() - по неизвестной причинеиклик отказался работать
        self.browser.execute_script("arguments[0].click();", delete_btn)
        try:
            confirm = self.browser.switch_to.alert
            confirm.accept()
        except:
            pass

    def get_value(self):
        return {'position': self.position.get_value(),
                'company_name': self.company_name.get_value(),
                'company_link': self.company_link.get_value(),
                'start_month': self.start_month.get_value(),
                'start_year': self.start_year.get_value(),
                'finish_month': self.finish_month.get_value(),
                'finish_year': self.finish_year.get_value(),
                'common_description': self.common_description.get_value(),
                'technologies': self.technologies.get_value(),
                }


class CompanySet:
    def __init__(self, browser):
        self.browser = browser
        self.companies = self.get_companies()
        self.add_element_btn = BaseElement(browser, CVEditFormExperience.ADD_COMPANY_BTN)
        self.count = self.get_number_of_rows()

    def add_companie(self):
        self.add_element_btn.click()
        self.companies.append(Company(self.browser, self.count))
        self.count += 1

    def delete_element(self, index=None):
        """удаляет пример кода со страницы, уменьшает счетчик примеров кода, удаляет значение из массива примеров кода"""
        if not index:
            index = self.get_number_of_rows() - 1
        element = self.companies[index]
        element.delete()
        self.companies.pop()  # при удалении любого элемента индексы оставшихся сдвигаются, т.е. всегда со страницы удалится элемент с наибольшим индексом
        self.count -= 1

    def get_number_of_rows(self):
        return len(self.browser.find_elements(*CVEditFormExperience.COMPANY_ROW))

    def clear(self):
        """если блок остается единственным на сранице, у него нет кнопки Удалить"""
        for _ in range(self.get_number_of_rows() - 1):
            self.delete_element()
        if self.companies:
            self.companies[0].clear()

    def clear_optional_fields(self):
        for _ in range(self.get_number_of_rows() - 1):
            self.delete_element()
        if self.companies:
            self.companies[0].clear_optional_fields()

    def fill_values(self):
        for element in self.companies:
            element.fill_values()

    def get_value(self):
        return sorted([element.get_value() for element in self.companies], key=lambda x: x['position'])

    def get_companies(self):
        return [Company(self.browser, n) for n in range(self.get_number_of_rows())]

    def should_be_correct_number_of_rows(self):
        assert self.count == self.get_number_of_rows(), f"number of other contact rows {self.count} is not correct {self.get_number_of_rows()}"

    def fill_input(self):
        self.clear()
        self.add_companie()
        self.fill_values()

