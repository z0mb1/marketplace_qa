from pages.base_page import BasePageCVEdit
from .form_elements import SelectGroup, Input, BaseElement
from .locators import CVEditFormPreferences, CVEditFormExperience
import random
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import settings


class CVEditPreferencesPage(BasePageCVEdit):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.preferences = SelectGroup(browser, CVEditFormPreferences.PREFERENCE_ITEM, settings.SELECTED_MAX_PREFERENCES)
        self.work_features = SelectGroup(browser, CVEditFormPreferences.WORK_FEATURE_ITEM,
                                         settings.SELECTED_MAX_WORK_FEATURES)
        self.detail = Input(browser, CVEditFormPreferences.DETAIL_INPUT, 'text')

    def get_values(self):
        return {'preferences': self.preferences.get_value(),
                'work_features': self.work_features.get_value(),
                'detail': self.detail.get_value()}

    def clear(self):
        self.preferences.clear_elements()
        self.work_features.clear_elements()
        self.detail.clear_value()

    def fill_values(self):
        self.clear()
        for _ in range(settings.SELECTED_MAX_PREFERENCES):
            self.preferences.choose_element()
        for _ in range(settings.SELECTED_MAX_WORK_FEATURES):
            self.work_features.choose_element()
        self.detail.fill_input()
