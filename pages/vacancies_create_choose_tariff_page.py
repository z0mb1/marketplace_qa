from .base_page import BasePage
from .locators import VacanciesCreateChooseTariffPageLocators


class VacanciesCreateChooseTariffPage(BasePage):

    def click_choose_tariff_btn(self, tariff):
        if tariff == 'Отклики':
            choose_tariff_btn = self.browser.find_element(*VacanciesCreateChooseTariffPageLocators.CREATE_TARIFF_RESPONSES_BTN)
        elif tariff == 'Робо-подбор':
            choose_tariff_btn = self.browser.find_element(*VacanciesCreateChooseTariffPageLocators.CREATE_TARIFF_ROBO_BTN)
        elif tariff == 'Про-подбор':
            choose_tariff_btn = self.browser.find_element(*VacanciesCreateChooseTariffPageLocators.CREATE_TARIFF_PRO_BTN)
        choose_tariff_btn.click()

    def close_wrapper(self):
        close_wrapper_btn = self.browser.find_element(*VacanciesCreateChooseTariffPageLocators.WRAPPER_CLOSE_BTN)
        close_wrapper_btn.click()