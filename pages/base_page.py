from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from .locators import CVEdit
from .form_elements import BaseElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    def open(self):
        self.browser.get(self.url)

    def refresh(self):
        """после вызова обновления страницы
           ожидается появление обновленной страницы.
           сравниваются id html документа до и после обновления.
           если обновление не выполнится, будет вызвана ошибка TimeoutException
        """
        old_page_id = self.browser.find_element(By.TAG_NAME, 'html').id
        self.browser.refresh()
        wait = WebDriverWait(self.browser, 10)
        wait.until(
            self.page_refreshed((By.TAG_NAME, 'html'), old_page_id)
        )
        print('page refresh done')

    class page_refreshed(object):
        """Кастомное ожидание для проверки обновления страницы
        сравнивается id html элемента до обновления и после
        locator - используется для поиска элемента (использвется тэг html)
        old_page_id - browser.find_element(By.TAG_NAME, 'html').id до обновления
        возвращает html новой страницы
        """

        def __init__(self, locator, old_page_id):
            self.locator = locator
            self.old_page_id = old_page_id

        def __call__(self, driver):
            # print('----------call check refresh')
            new_page = driver.find_element(*self.locator)
            # print(self.old_page_id, new_page.id)
            if self.old_page_id != new_page.id:
                return new_page
            else:
                return False

    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

    def should_be_url(self, url):
        #print(url.rstrip('/'))
        #print(self.browser.current_url.rstrip('/'))
        browser_url = self.browser.current_url
        assert url.rstrip('/') == browser_url.rstrip('/'), f"{url} link not equal current browser url {browser_url}"

    def should_show_text(self, text):
        assert self.is_element_present(
            By.XPATH, f"//*[contains(text(), '{text}')]"), f"text {text} is not present on page"

    def get_element_text(self, how, what):
        #print(self.browser.find_element(how, what).text)
        return self.browser.find_element(how, what).text


class BasePageCVEdit(BasePage):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        self.save_btn = BaseElement(browser, CVEdit.SAVE_BTN)

    def save(self):
        self.save_btn.click()

    def should_show_success_message(self):
        try:
            WebDriverWait(self.browser, 5).until(
                EC.presence_of_element_located(CVEdit.SAVE_SUCCESS)
            )
        except:
            pass
        assert self.is_element_present(*CVEdit.SAVE_SUCCESS), 'should show success message for success saving'


