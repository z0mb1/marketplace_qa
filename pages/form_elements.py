from selenium.common.exceptions import NoSuchElementException
import settings
import os
import random
import time
from selenium.webdriver.common.keys import Keys


class BaseElement:

    def __init__(self, browser, selector, is_required=False):
        self.browser = browser
        assert self.is_element_present(*selector), f"element with selector '{selector}' is not present on current page"
        self.element = browser.find_element(*selector)
        self.is_required = is_required
        self.value = None

    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

    def click(self):
        self.element.click()


class Input(BaseElement):
    def __init__(self, browser, input_selector, input_type, is_required=False):
        super().__init__(browser, input_selector, is_required)
        self.type = input_type

    def fill_input(self, value=None):
        if self.type == 'text':
            #self.clear_value()
            self.element.send_keys(value if value else settings.random_string())
        elif self.type == 'number':
            #self.element.clear()
            self.element.send_keys(value if value else random.randint(1000, 100000))
        elif self.type == 'file':
            if self.get_value() == '':
                self.element.send_keys(os.path.join(settings.MEDIA_ROOT, value))
            else:
                print('image is already imported')

    def get_value(self):
        return self.element.get_attribute('value')

    def clear_value(self):
        #self.element.send_keys(Keys.ENTER)
        self.element.send_keys(Keys.CONTROL + "a" + Keys.DELETE)
        #self.element.clear()


class DropDown(BaseElement):
    def __init__(self, browser, dropdown_selector, element_selector, index=None, is_required=False):
        super().__init__(browser, dropdown_selector, is_required)
        self.dropdown = self.browser.find_element(*dropdown_selector)
        self.elements = self.browser.find_elements(*element_selector)

    def choose_value(self, value='random'):
        self.dropdown.click()
        if value == 'random':
            element = random.choice(self.elements)
        else:
            element = self.elements[value]
        element.click()

    def get_value(self):
        if 'Выберите' in self.dropdown.get_attribute('title'):
            return None
        return self.dropdown.get_attribute('title')


class SwitchStatusElement(BaseElement):
    def __init__(self, browser, switch_btn_selector):
        super().__init__(browser)
        self.switch_btn = self.browser.find_element(*switch_btn_selector)
        self.status = self.get_status()

    def get_status(self):
        if self.switch_btn.get_attribute('aria-selected') == 'false':
            return False
        elif self.switch_btn.get_attribute('aria-selected') == 'true':
            return True

    def click(self):
        self.switch_btn.click()
        self.status = not self.status

    def is_status_correct(self):
        assert self.status == self.get_status(), 'status is not correct'


class SingleSelect(BaseElement):

    def __init__(self, browser, switch_btn_selector, is_required=False):
        super().__init__(browser, switch_btn_selector, is_required)
        self.status = self.get_value()

    def get_value(self):
        if self.element.get_attribute('aria-selected') == 'false':
            return False
        elif self.element.get_attribute('aria-selected') == 'true':
            return True

    def make_selected(self):
        if not self.get_value():
            self.click()

    def click(self):
        self.element.click()
        self.status = not self.status

    def is_status_correct(self):
        assert self.status == self.get_value(), 'status is not correct'


class ExtraFieldsElement(BaseElement):

    def __init__(self, browser, switch_btn_selector, *extra_field_selectors):
        super().__init__(browser)
        self.switch_btn = self.browser.find_element(*switch_btn_selector)
        self.status = self.get_status()
        self.extra_field_selectors = extra_field_selectors

    def get_status(self):
        if self.switch_btn.get_attribute('aria-selected') == 'false':
            return False
        elif self.switch_btn.get_attribute('aria-selected') == 'true':
            return True

    def click(self):
        self.switch_btn.click()
        self.status = not self.status

    def is_status_correct(self):
        assert self.status == self.get_status(), 'status is not correct'

    def should_show_extra_fields(self):
        for extra_field_selector in self.extra_field_selectors:
            if self.status:
                assert self.is_element_present(*extra_field_selector), 'extra field is not present'
            else:
                assert not self.is_element_present(*extra_field_selector), 'extra field is present'


class Tab:
    def __init__(self, browser, left_element_selector, right_element_selector):
        self.browser = browser
        self.left = BaseElement(browser, left_element_selector)
        self.right = BaseElement(browser, right_element_selector)
        self.selected = self.get_selected()

    def get_value(self):
        return self.get_selected().element.text

    def get_selected(self):
        if self.left.element.get_attribute('class').endswith('active'):
            return self.left
        elif self.right.element.get_attribute('class').endswith('active'):
            return self.right

    def click_unselected_value(self):
        if self.selected == self.left:
            self.right.click()
            self.selected = self.right
        elif self.selected == self.right:
            self.left.click()
            self.selected = self.left
        #time.sleep(3)

    def should_be_correct_selected_value(self):
        # сравнивает значение, хранящееся в атрибуте объекта и изменяющееся по клику с реальным значением атрибута элемента на странице
        # print(self.checked, self.is_checked())
        assert self.selected == self.get_selected(), 'wrong checked value'


class CheckBox(BaseElement):

    def __init__(self, browser, input_selector, clickable_element_selector, is_required=False):
        super().__init__(browser, clickable_element_selector, is_required)
        self.input = self.browser.find_element(*input_selector)
        self.checked = True if self.input.get_attribute("checked") == 'true' else False

    def get_value(self):
        return self.is_checked()

    def click(self):
        self.element.click()
        self.checked = not self.checked
        #time.sleep(3)

    def should_be_correct_checked_value(self):
        # сравнивает значение, хранящееся в атрибуте объекта и изменяющееся по клику с реальным значением атрибута элемента на странице
        #print(self.checked, self.is_checked())
        assert self.checked == self.is_checked(), f'wrong checkbox value: {self.checked } != {self.is_checked()}'

    def is_checked(self):
        if self.input.get_attribute("checked") == 'true':
            return True
        return False


class SelectGroup:
    def __init__(self, browser, elements_selector, max_elements):
        self.elements = browser.find_elements(*elements_selector)
        assert len(self.elements) > 0, "select elements is not present on current page"
        self.number_of_elements = len(self.elements)
        self.clicked_elements = []
        self.max_elements = max_elements
        self.browser = browser

    def get_value(self):
        res = []
        for element in self.elements:
            if self.is_element_selected(element):
                res.append(element.text)
        return res

    def clear_elements(self):
        for element in self.elements:
            if self.is_element_selected(element):
                #element.click()
                self.browser.execute_script("arguments[0].click();", element)
        self.clicked_elements = []

    def is_element_selected(self, element):
        selected = element.get_attribute("aria-selected")
        if selected == 'true':
            return True
        return False

    def click_element(self, index_of_element):
        #time.sleep(3)
        el = self.elements[index_of_element]
        # el.click() при запуске тест кейсов не работает в этом тесте, в ручном режиме работает
        self.browser.execute_script("arguments[0].click();", el)
        #time.sleep(3)

    def choose_element(self, random_choice=True, i=None):
        if len(self.clicked_elements) >= self.number_of_elements:
            raise AssertionError("All elements are already chosen")
        if random_choice:
            i = random.choice(list(set(range(self.number_of_elements)) - set(self.clicked_elements)))
        #print('choose', i, self.elements[i].text)
        #print(f'clicked to index{i}')
        self.click_element(i)
        # если уровень был выбран ранее, проверяем, что после клика элемент не выбран
        if i in self.clicked_elements:
            self.clicked_elements.remove(i)
            assert not self.is_element_selected(
                self.elements[i]), 'element is still selected after canceled'
        # если элемент не был выбран ранее
        else:
            #print(self.clicked_elements)
            # если превышен лимит, то проверяем, что новый элемент не выбирается
            if len(self.clicked_elements) >= self.max_elements:
                assert not self.is_element_selected(self.elements[i]), 'element selected over max value'
            # добавляем его в список выбраных и проверяем, что элемент выбран
            else:
                self.clicked_elements.append(i)
                assert self.is_element_selected(self.elements[i]), 'element is not selected'


class MultiSelectWithAutoComplete(BaseElement):
    """element_selector и delete_element_selector представляют собой селектор внутри lambda функции,
        в которую передается индекс элемента"""
    def __init__(self, browser, input_selector, elements_group_selector, element_selector, delete_element_selector, is_required=False):
        super().__init__(browser, input_selector, is_required)
        self.count_elements_selector = elements_group_selector
        self.element_selector = element_selector
        self.delete_element_selector = delete_element_selector
        self.elements = self.get_elements()
        self.count = self.get_number_of_items()

    def add_element(self, value=None):
        #print('count', self.count)
        if not value:
            value = random.choice('abcdefghijklmnopqrstuvwxyz')
        self.element.send_keys(value)
        time.sleep(1)
        self.element.send_keys(Keys.ENTER)
        index = self.count
        self.elements.append(Element(self.browser, self.element_selector(index), self.delete_element_selector(index)))
        self.count += 1

    def get_number_of_items(self):
        return len(self.browser.find_elements(*self.count_elements_selector))

    def get_elements(self):
        elements = []
        for index in range(self.get_number_of_items()):
            elements.append(Element(self.browser, self.element_selector(index), self.delete_element_selector(index)))
        return elements

    def delete_element(self, index=None):
        if not index:
            index = self.get_number_of_items() - 1
        #print('index', index)
        element = self.elements[index]
        element.delete()
        self.elements.remove(element)
        self.count -= 1

    def get_value(self):
        return set([el.get_value() for el in self.elements])

    def clear(self):
        for _ in range(self.get_number_of_items()):
            self.delete_element()

    def should_be_correct_number_of_items(self):
        assert self.count == self.get_number_of_items(), 'number of skill rows is not correct'

    def fill_input(self):
        self.clear()
        self.add_element()
        self.add_element()


class Element(BaseElement):
    def __init__(self, browser, element_selector, delete_element_selector, is_required=False):
        super().__init__(browser, element_selector, is_required)
        self.delete_element_btn = browser.find_element(*delete_element_selector)
        self.delete_selector = delete_element_selector

    def delete(self):
        #print('del element')
        #print(self.delete_selector)
        self.browser.find_element(*self.delete_selector).click()
        #self.delete_element_btn.click()

    def get_value(self):
        return self.element.text


class CityWithAutocomplete(BaseElement):

    def __init__(self, browser, input_selector, value_selector, is_required=False):
        super().__init__(browser, input_selector, is_required)
        self.value_selector = value_selector

    def fill_value(self, value=None):
        value = random.choice('abcdefghijklmnopqrstuvwxyz')
        self.element.send_keys(value)
        time.sleep(1)
        self.element.send_keys(Keys.ENTER)

    def get_value(self):
        return self.browser.find_element(*self.value_selector).text

class TextInputWithAutocomplete(BaseElement):

    def __init__(self, browser, input_selector, value_selector, is_required=False):
        super().__init__(browser, input_selector, is_required)
        self.value_selector = value_selector

    def fill_input(self, value=None):
        value = random.choice('abcdefghijklmnopqrstuvwxyz')
        self.element.send_keys(value)
        time.sleep(1)
        self.element.send_keys(Keys.ENTER)

    def get_value(self):
        return self.browser.find_element(*self.value_selector).text
