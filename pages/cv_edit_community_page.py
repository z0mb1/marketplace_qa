from pages.base_page import BasePageCVEdit
from .form_elements import Input, BaseElement, DropDown, Tab, CheckBox, SelectGroup, SingleSelect, MultiSelectWithAutoComplete
from .locators import CVEditFormCommunity, CVEditFormPreferences
import random
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


class CVEditCommunityPage(BasePageCVEdit):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.github = Input(browser, CVEditFormCommunity.GITHUB_INPUT, 'text')
        self.gitlab = Input(browser, CVEditFormCommunity.GITLAB_INPUT, 'text')
        self.stackoverflow = Input(browser, CVEditFormCommunity.STACKOVERFLOW_INPUT, 'text')
        self.habr = Input(browser, CVEditFormCommunity.HABR_INPUT, 'text')
        self.bitbucket = Input(browser, CVEditFormCommunity.BUTBUCKET_INPUT, 'text')
        self.toster = Input(browser, CVEditFormCommunity.TOSTER_INPUT, 'text')
        self.code_examples = CodeExampleSet(browser)

    def fill_community_inputs(self):
        self.github.fill_input()
        self.gitlab.fill_input()
        self.stackoverflow.fill_input()
        self.habr.fill_input()
        self.bitbucket.fill_input()
        self.toster.fill_input()
        self.code_examples.fill_input()

    def clear(self):
        self.github.clear_value()
        self.gitlab.clear_value()
        self.stackoverflow.clear_value()
        self.habr.clear_value()
        self.bitbucket.clear_value()
        self.toster.clear_value()
        self.code_examples.clear()


    def get_values(self):
        github = self.github.get_value()
        gitlab = self.gitlab.get_value()
        stackoverflow = self.stackoverflow.get_value()
        habr = self.habr.get_value()
        bitbucket = self.bitbucket.get_value()
        toster = self.toster.get_value()
        return {'github': github if github.startswith('http') else "https://github.com/" + github,
                'gitlab': gitlab if gitlab.startswith('http') else "https://gitlab.com/" + gitlab,
                'stackoverflow': stackoverflow if stackoverflow.startswith('http') else "https://stackoverflow.com/users/" + stackoverflow,
                'habr': habr if habr.startswith('http') else "https://habr.com/users/" + habr,
                'bitbucket': bitbucket if bitbucket.startswith('http') else "https://bitbucket.org/" + bitbucket,
                'toster': toster if toster.startswith('http') else"https://toster.ru/user/" + toster,
                'code_examples': self.code_examples.get_value()
                }

class CodeExample:
    def __init__(self, browser, index):
        self.browser = browser
        self.index = index
        self.code_example_link = Input(browser, CVEditFormCommunity.CODE_EXAMPLE_LINK(index), 'text')
        self.technologies = MultiSelectWithAutoComplete(browser,
                                                        CVEditFormCommunity.TECHNOLOGIES_INPUT(index),
                                                        CVEditFormCommunity.TECHNOLOGIES_ELEMENTS(index),
                                                        # если кто-то это читает, не пугайтесь, это просто селектор внутри lambda, обернутый в другую lambda
                                                        # так надо
                                                        lambda index_el: CVEditFormCommunity.TECHNOLOGIES_ELEMENT(index, index_el),
                                                        lambda index_el: CVEditFormCommunity.TECHNOLOGIES_DELETE_ELEMENT(index, index_el))
        self.code_description = Input(browser, CVEditFormCommunity.CODE_DESCRIPTION(index), 'text')
        #self.delete_btn = self.browser.find_element(*CVEditFormPersonal.DELETE_CONTACT_BTN(index))

    def make_contact_set(self):
        self.code_example_link.fill_input()
        self.technologies.fill_input()
        self.code_description.fill_input()

    def clear(self):
        self.code_example_link.clear_value()
        self.technologies.clear()
        self.code_description.clear_value()

    def delete(self):
        delete_btn = self.browser.find_element(*CVEditFormCommunity.DELETE_CODE_EXAMPLE_BTN(self.index))
        # delete_btn.click() - по неизвестной причинеиклик отказался работать
        self.browser.execute_script("arguments[0].click();", delete_btn)

    def get_value(self):
        return {'code_example_link': self.code_example_link.get_value(),
                'technologies': self.technologies.get_value(),
                'code_description': self.code_description.get_value()}


class CodeExampleSet:
    def __init__(self, browser):
        self.browser = browser
        self.code_examples = self.get_code_examples()
        self.add_element_btn = BaseElement(browser, CVEditFormCommunity.ADD_CODE_EXAMPLE_BTN)
        self.count = self.get_number_of_rows()

    def add_other_contact(self):
        self.add_element_btn.click()
        self.code_examples.append(CodeExample(self.browser, self.count))
        self.count += 1

    def delete_element(self, index=None):
        """удаляет пример кода со страницы, уменьшает счетчик примеров кода, удаляет значение из массива примеров кода"""
        if not index:
            index = self.get_number_of_rows() - 1
        element = self.code_examples[index]
        element.delete()
        self.code_examples.pop()  # при удалении любого элемента индексы оставшихся сдвигаются, т.е. всегда со страницы удалится элемент с наибольшим индексом
        self.count -= 1

    def get_number_of_rows(self):
        return len(self.browser.find_elements(*CVEditFormCommunity.CODE_EXAMPLE_ROW))

    def clear(self):
        """если блок остается единственным на сранице, у него нет кнопки Удалить"""
        for _ in range(self.get_number_of_rows() - 1):
            self.delete_element()
        self.code_examples[0].clear()

    def fill_values(self):
        for element in self.code_examples:
            element.make_contact_set()

    def get_value(self):
        return [element.get_value() for element in self.code_examples]

    def get_code_examples(self):
        return [CodeExample(self.browser, n) for n in range(self.get_number_of_rows())]

    def should_be_correct_number_of_rows(self):
        assert self.count == self.get_number_of_rows(), f"number of other contact rows {self.count} is not correct {self.get_number_of_rows()}"

    def fill_input(self):
        self.clear()
        self.add_other_contact()
        self.fill_values()
