from .base_page import BasePage
from .locators import Vacancy
import re


class VacancyPage(BasePage):

    def __init__(self, browser, url):
        super().__init__(browser, url)

        self.vacancy_data = {'name': self.get_name(),
                             'salary': self.get_salary(),
                             'work_type': self.get_office_or_remote(),
                             'address': self.get_address(),
                             'has_relocation_support': self.get_has_relocation_support(),
                             'is_skills_required': self.get_is_skills_required(),
                             'seniority': self.get_seniority(),
                             'skills': self.get_skills(),
                             'tasks': self.get_tasks(),
                             'other_skills': self.get_other_skills(),
                             'conditions': self.get_conditions(),
                             'options_and_bonuses': self.get_options_and_bonuses(),
                             'technologies_and_services': self.get_using_technologies(),
                             'team_description': self.get_team_description()
                             }

    def get_vacancy_data(self):
        pass

    def get_name(self):
        return self.browser.find_element(*Vacancy.NAME).text

    def get_salary(self):
        salary_result = {'show_salary': None, 'salary_from': None, 'salary_to': None, 'currency': None, 'period': None}
        if not self.is_element_present(*Vacancy.SALARY):
            salary_result['show_salary'] = False
            return salary_result
        salary_result['show_salary'] = True
        salary_pattern = re.compile(r'((?P<salary_from>\d*)–(?P<salary_to>\d*)(?P<currency>.))')
        salary_from_page = re.search(salary_pattern, ''.join(self.browser.find_element(*Vacancy.SALARY).text.split()))
        salary_result['salary_from'] = salary_from_page.group('salary_from')
        salary_result['salary_to'] = salary_from_page.group('salary_to')
        salary_result['currency'] = salary_from_page.group('currency')
        return salary_result

    def get_office_or_remote(self):
        work_type = {'office': False, 'remote': False}
        office_or_remote = self.browser.find_element(*Vacancy.OFFICE_OR_REMOTE).text
        if re.search('офис', office_or_remote.lower()):
            work_type['office'] = True
        if re.search('удаленно', office_or_remote.lower()):
            work_type['remote'] = True
        return work_type

    def get_address(self):
        work_type = self.get_office_or_remote()
        if work_type['office']:
            return self.browser.find_element(*Vacancy.OFFICE_ADDRESS).text
        return

    def get_has_relocation_support(self):
        work_type = self.get_office_or_remote()
        if work_type['office']:
            return self.is_element_present(*Vacancy.HAS_RELOCATION_SUPPORT)
        return None

    def get_is_skills_required(self):
        return self.is_element_present(*Vacancy.IS_SKILLS_REQUIRED)

    def get_seniority(self):
        seniority_categories = ['Junior', 'Middle', 'Senior', 'Lead']
        seniority = []
        seniority_string = self.browser.find_element(*Vacancy.SENIORITY).text
        for cat in seniority_categories:
            if cat in seniority_string:
                seniority.append(cat)
        return seniority

    def get_skills(self):
        skills = []
        areas = self.browser.find_elements(*Vacancy.AREA)
        technologies = self.browser.find_elements(*Vacancy.TECHNOLOGY)
        experiences = self.browser.find_elements(*Vacancy.EXPERIENCE)
        for a, t, e in zip(areas, technologies, experiences):
            skills.append({'area': a.text, 'technology': t.text, 'experience': e.text.split()[1]})
        return skills

    def get_tasks(self):
        return self.browser.find_element(*Vacancy.TASKS).text

    def get_other_skills(self):
        if not self.is_element_present(*Vacancy.OTHER_SKILLS):
            return
        return self.browser.find_element(*Vacancy.OTHER_SKILLS).text

    def get_conditions(self):
        if not self.is_element_present(*Vacancy.CONDITIONS):
            return
        return self.browser.find_element(*Vacancy.CONDITIONS).text

    def get_options_and_bonuses(self):
        options_and_bonuses = []
        for option in self.browser.find_elements(*Vacancy.OPTIONS_AND_BONUSES):
            options_and_bonuses.append(option.text)
        return options_and_bonuses

    def get_using_technologies(self):
        using_technologies = []
        for option in self.browser.find_elements(*Vacancy.USING_TECHNOLOGIES):
            using_technologies.append(option.text)
        return using_technologies

    def get_team_description(self):
        if not self.is_element_present(*Vacancy.TEAM_DESCRIPTION):
            return
        return self.browser.find_element(*Vacancy.TEAM_DESCRIPTION).text
