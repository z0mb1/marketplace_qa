from .base_page import BasePage
from .locators import LoginPageLocators


class LoginPage(BasePage):

    def should_be_email_and_password_inputs(self):
        self.should_be_email_input()
        self.should_be_password_input()

    def should_be_email_input(self):
        assert self.is_element_present(*LoginPageLocators.EMAIL_INPUT), "Login input is not presented"

    def should_be_password_input(self):
        assert self.is_element_present(*LoginPageLocators.PASSWORD_INPUT), "Password input is not presented"

    def should_be_wrong_input_alert(self):
        assert self.is_element_present(*LoginPageLocators.WRONG_INPUTS_ALERT), "Wrong input alert is not presented"

    def login(self, email, password):
        email_input = self.browser.find_element(*LoginPageLocators.EMAIL_INPUT)
        email_input.send_keys(email)
        password_input = self.browser.find_element(*LoginPageLocators.PASSWORD_INPUT)
        password_input.send_keys(password)
        login_btn = self.browser.find_element(*LoginPageLocators.LOGIN_BTN)
        login_btn.click()