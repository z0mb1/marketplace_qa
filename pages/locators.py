from selenium.webdriver.common.by import By


class LoginPageLocators:
    EMAIL_INPUT = (By.XPATH, "//input[@type='email']")
    PASSWORD_INPUT = (By.XPATH, "//input[@type='password']")
    LOGIN_BTN = (By.XPATH, "//button[@type='submit']")
    WRONG_INPUTS_ALERT = (By.CLASS_NAME, 'invalid-feedback')


class VacanciesPageLocators:
    CREATE_VACANCY_BTN = (By.XPATH, "//button/*[text()='Создать вакансию']")
    TITLE_CONTAINS_TEXT = "Вакансии"


class VacanciesCreateChooseTariffPageLocators:
    CREATE_TARIFF_PRO_BTN = (By.XPATH, "//div/h4[text()='Про-подбор']/../../..//button")
    CREATE_TARIFF_ROBO_BTN = (By.XPATH, "//div/h4[text()='Робо-подбор']/../../..//button")
    CREATE_TARIFF_RESPONSES_BTN = (By.XPATH, "//div/h4[text()='Отклики']/../../..//button")
    WRAPPER = (By.CLASS_NAME, 'geecko-modal-root')
    WRAPPER_CLOSE_BTN = (By.CLASS_NAME, 'geecko-tariffs-offrer-modal__close-wrapper')
    TITLE_CONTAINS_TEXT = "Создание вакансии"


class VacanciesCreateForm:
    NAME_INPUT = (By.NAME, 'name')
    QUANTITY_INPUT = (By.NAME, 'quantity')
    CHOOSE_CURRENCY_CONTAINER = (By.NAME, 'salary.currency')
    CURRENCY_RUB = (By.XPATH, "//div[text()='₽']")
    CURRENCY_USD = (By.XPATH, "//div[text()='$']")
    CHOOSE_PERIOD_CONTAINER = (By.NAME, 'salary.period')
    PERIOD_MONTH = (By.XPATH, "//div[text()='Мес']")
    PERIOD_YEAR = (By.XPATH, "//div[text()='Год']")
    # salary
    SALARY_FROM_INPUT = (By.NAME, 'salary.from')
    SALARY_TO_INPUT = (By.NAME, 'salary.to')
    SALARY_RATIO_ERROR = (By.XPATH, "//div[text()='Значение не может быть меньше предыдущего']")
    SALARY_IS_VISIBLE_INPUT = (
    By.XPATH, "//input[@id='checkbox_salary.is_visible']")  # не кликабельный, содержит атрибут checked
    SALARY_IS_VISIBLE_CHECKBOX = (
    By.XPATH, "//input[@id='checkbox_salary.is_visible']/..")  # кликабельная обертка чекбокса
    # work office
    WORK_TYPE_OFFICE = (By.XPATH, "//li[@value='office']")
    HAS_RELOCATION_SUPPORT_INPUT = (
        By.XPATH, "//input[@id='checkbox_has_relocation_support']")  # не кликабельный, содержит атрибут checked
    HAS_RELOCATION_SUPPORT_CHECKBOX = (
        By.XPATH, "//input[@id='checkbox_has_relocation_support']/..")  # кликабельная обертка чекбокса
    OFFICE_ADDRESS_DROPDOWN = (By.NAME, "office.id")
    ADD_NEW_ADDRESS = (By.XPATH, "//li[@value='add']")
    NEW_ADDRESS_INPUT = (By.NAME, "office.dadata_address")
    # work remote
    WORK_TYPE_REMOTE = (By.XPATH, "//li[@value='remote']")
    TIMEZONE_FIELDSET = (By.CLASS_NAME, "geecko-timezone-range__range-container")

    IS_SKILLS_REQUIRED_INPUT = (
        By.XPATH, "//input[@id='switch_tech.is_skills_required']")  # не кликабельный, содержит атрибут checked
    IS_SKILLS_REQUIRED_CHECKBOX = (
        By.XPATH, "//input[@id='switch_tech.is_skills_required']/..")  # кликабельная обертка чекбокса
    SENIORITY_CONTAINER = (By.XPATH, "//input[@name='tech.seniority']/following-sibling::div")
    SENIORITY_LEVEL = (By.XPATH, "//input[@name='tech.seniority']/following-sibling::div//li")
    SPECIALIZATION_CONTAINER = (By.XPATH, "//input[@name='tech.specializations']/following-sibling::div")
    SPECIALIZATION_ITEM = (By.XPATH, "//input[@name='tech.specializations']/following-sibling::div//li")
    # skills
    AREA_DROPDOWN = lambda index: (By.NAME, f"tech.skillset[{index}].skill_area")
    AREA_VALUE = lambda index: (By.XPATH,
                                f"//button[@name='tech.skillset[{index}].skill_area']/following-sibling::ul/li")  # может быть больше одного варианта

    TECHNOLOGY_DROPDOWN = lambda index: (By.NAME, f"tech.skillset[{index}].skill_technology")
    TECHNOLOGY_VALUE = lambda index: (By.XPATH,
                                      f"//button[@name='tech.skillset[{index}].skill_technology']/following-sibling::ul/li")  # может быть больше одного варианта
    EXPERIENCE_INPUT = lambda index: (By.NAME, f"tech.skillset[{index}].experience_from_month")
    DELETE_SKILL_BTN = (By.CLASS_NAME, "skill-set-select__delete-button")  # может быть больше одного объекта
    ADD_TECHNOLOGY_BTN = (By.XPATH, "//div[text()='Добавить технологию']/..")
    CHOOSE_SKILL_CHECKBOX = (
    By.XPATH, "//div[contains(@class, 'geecko-black-checkbox')]")  # может быть больше одного объекта
    SKILL_ROW = (By.XPATH, "//table[contains(@class, 'skill-set-select')]/tbody/tr")  # может быть больше одного объекта
    GROUP_TECHNOLOGIES_WITH_OR_STATEMENT_BTN = (By.XPATH, "//div[text()='В группу «ИЛИ»']/..")
    UNGROUP_TECHNOLOGIES_BTN = (By.CLASS_NAME, "skill-set-select__ungroup-button")  # может быть больше одного объекта

    LANGUAGES_REQUIRED_INPUT = (By.ID, "switch_languagesisset")  # не кликабельный, содержит атрибут checked
    LANGUAGES_REQUIRED_CHECKBOX = (By.XPATH, "//input[@id='switch_languagesisset']/..")  # кликабельная обертка чекбокса
    LANGUAGES_SET_CONTAINER = (By.XPATH, "//span[text()='Иностранные языки']/../..")
    CHOOSE_LANGUAGE_DROPDOWN = (By.NAME, "languages[0].id")
    CHOOSE_LEVEL_DROPDOWN = (By.XPATH, "//button[@name='languages[0].level']/..")
    DELETE_LANGUAGE_BTN = (By.XPATH,
                           "//span[text()='Иностранные языки']/../..//button[contains(@class, 'geecko-iconbutton')]")  # может быть больше одного объекта
    ADD_LANGUAGE_BTN = (By.XPATH, "//span[text()='Добавить язык']/..")
    ABOUT_COMPANY_TEXTAREA = (By.XPATH, "//textarea[contains(@placeholder, 'о компании')]")
    TASKS_TEXTAREA = (By.XPATH, "//textarea[contains(@placeholder, 'задачи')]")
    USING_TECHNOLOGIES_TEXTAREA = (
        By.XPATH, "//span[contains(text(), 'технологии и сервисы')]/../..//div[contains(text(), 'Например')]/../..")
    OTHER_SKILLS_TEXTAREA = (By.XPATH, "//textarea[contains(@placeholder, 'Какие еще знания требуются')]")
    CONDITIONS_TEXTAREA = (By.XPATH, "//textarea[contains(@placeholder, 'об условиях')]")
    OPTIONS_AND_BONUSES_MULTISELECT = (By.XPATH, "//span[text()='Опции и бонусы']/../..//div")
    SELECT_TEAM_CHECKBOX = (By.XPATH, "//span[contains(text(), 'Указать команду')]/..")
    # название команды
    # описание команды
    COMPANY_RESPONSIBLE_DROPDOWN = (By.NAME, "company_responsible_id")
    IS_RESPONSIBLE_VISIBLE_CHECKBOX = (By.XPATH, "//input[@id='checkbox_is_responsible_visible']/..")
    SUBMIT_VACANCY_BTN = (By.XPATH, "//button[@type='submit']")


class Vacancy:
    TITLE_CONTAINS_TEXT = 'Вакансия'
    NAME = (By.TAG_NAME, 'h1')
    # QUANTITY = (By.NAME, 'quantity') не отображается или не отображается для 1 человека (надо проверить)
    SALARY = (By.XPATH, "//div[contains(text(), '₽') or contains(text(), '$') ]")
    # PERIOD = не указан, надо доработать фронт
    # IS_VISIBLE если выбрано "не показывать, то не будет найдет элемент с селектором salary
    OFFICE_OR_REMOTE = (
    By.XPATH, "//*[contains(text(), 'Офис') or contains(text(), 'Удаленно') or contains(text(),'Офис или удаленно')]")
    OFFICE_ADDRESS = (By.XPATH,
                      "//*[text()='Адрес офиса']/following-sibling::*")  # если выбрано в вакансии удаленно, то будет показывать адрес, указанный в профиле компании
    HAS_RELOCATION_SUPPORT = (
    By.XPATH, "//div[text()='Помогаем с переездом']")  # если не выбрано, то элемент не будет присутствовать на странице
    # TIMEZONE = (By.CLASS_NAME, "geecko-timezone-range__range-container") информации нет на странице, надо поправить

    IS_SKILLS_REQUIRED = (By.XPATH,
                          "//*[contains(text(), 'тестированию Geecko')]")  # если не выбрано, то элемент не будет присутствовать на странице

    SENIORITY = (By.XPATH,
                 "//*[contains(text(), 'Junior') or contains(text(), 'Middle') or contains(text(),'Senior') or contains(text(),'Lead')]")

    # SPECIALIZATION = не представлена информация на странице
    # skills
    AREA = (By.XPATH, "//*[@class='geecko-skillset__group-title']")  # может быть несколько элементов
    TECHNOLOGY = (By.XPATH, "//*[@class='geecko-candidate-stacktag__label']")  # может быть несколько элементов
    EXPERIENCE = (By.XPATH, "//*[@class='geecko-candidate-stacktag__experience']")  # может быть несколько элементов

    # LANGUAGES = нет уникального селектора, можно будет искать по конкретному выбранному языку
    # ABOUT_COMPANY = нет уникального селектора
    TASKS = (By.XPATH, "//text()[contains(., 'Что предстоит делать')]/../following-sibling::*")
    OTHER_SKILLS = (By.XPATH, "//text()[contains(., 'Что нужно уметь')]/../following-sibling::*")
    CONDITIONS = (By.XPATH, "//text()[contains(., 'Условия и преимущества')]/../following-sibling::*")
    OPTIONS_AND_BONUSES = (
    By.XPATH, "//text()[contains(., 'Опции и бонусы')]/../following-sibling::*/div")  # может быть несколько элементов
    USING_TECHNOLOGIES = (By.XPATH,
                          "//text()[contains(., 'Мы используем технологии и сервисы')]/../following-sibling::*/div")  # может быть несколько элементов
    TEAM_DESCRIPTION = (By.XPATH, "//text()[contains(., 'Команда')]/../following-sibling::*")
    # COMPANY_RESPONSIBLE = нет уникатьного селектора


class Jobs:
    TITLE_CONTAINS_TEXT = "Вакансии"


class CVEdit:
    """общие для всех страниц редактирования резюме селекторы"""
    #
    SAVE_BTN = (By.XPATH, "//button[@type='submit']")
    SAVE_SUCCESS = (By.XPATH, "//*[contains(text(), 'Данные успешно сохранены')]")


class CVEditFormPersonal:
    TITLE_CONTAINS_TEXT = "Персональная информация"
    # Фото профиля
    AVATAR_INPUT = (By.ID, 'avatar-image')
    AVATAR_REMOVE = (By.CLASS_NAME, "geecko-input-image__remove")
    AVATAR_CONTAINER = (By.CLASS_NAME, 'geecko-input-image__label')
    AVATAR_IMAGE = (By.CLASS_NAME, "geecko-input-image__image")
    # Имя и фамилия
    FULLNAME_INPUT = (By.NAME, 'fullName')
    MISSING_FULLNAME_MESSAGE = 'Заполните поле'
    # Эл. почта
    EMAIL_INPUT = (By.NAME, 'email')
    MISSING_EMAIL_MESSAGE = 'Введите корректный email'
    # Телефон
    PHONE_INPUT = (By.ID, 'phone-form-control')
    MISSING_PHONE_MESSAGE = 'Введите настоящий номер телефона'
    # Дата рождения
    BIRTH = (By.NAME, "birthDay")
    # Прочие контакты
    ADD_CONTACT_BTN = (By.XPATH, "//*[contains(@class, 'qa-add-contact-button')]/button")
    CONTACT_ROW = (By.XPATH, "//*[contains(@class, 'qa-contact-container')]")  # может быт несколько элементов
    CONTACT_TYPE = lambda index: (By.NAME, f"contacts[{index}].channelId")
    CONTACT_TYPE_ELEMENT = lambda index: (
    By.XPATH, f"//button[@name='contacts[{index}].channelId']/following-sibling::ul/li")
    CONTACT_VALUE = lambda index: (By.NAME, f"contacts[{index}].value")
    DELETE_CONTACT_BTN = lambda index: (
        By.XPATH, f"//*[contains(@class, 'qa-contact-container')][{index+1}]//*[contains(@class, 'qa-delete-contact-button')]/button")


class CVEditFormCommon:
    TITLE_CONTAINS_TEXT = "Главная информация"
    CV_NAME_INPUT = (By.NAME, "profession")
    ABOUT_INPUT = (By.XPATH, "//*[text()='О себе']/../following-sibling::textarea")
    CURRENCY_RUB = (By.XPATH, "//*[text()='₽']")
    CURRENCY_USD = (By.XPATH, "//*[text()='$']")
    PERIOD_MONTH = (By.XPATH, "//*[text()='Мес']")
    PERIOD_YEAR = (By.XPATH, "//*[text()='Год']")
    SALARY_MIN_INPUT = (By.NAME, "salary.min")
    SALARY_PUBLIC_INPUT = (By.NAME, "salary.public")
    SALARY_SHOW_INPUT = (
        By.XPATH, "//input[@id='switch_salary.isVisible']")  # не кликабельный, содержит атрибут checked
    SALARY_SHOW_CHECKBOX = (
        By.XPATH, "//input[@id='switch_salary.isVisible']/..")  # кликабельная обертка чекбокса
    # Ваш город
    CITY_INPUT = (By.XPATH, "//*[text()='Ваш город']/../following-sibling::div//input[contains(@id, 'react-select')]")
    CITY_VALUE = (By.XPATH, "//div[contains(@class, 'singleValue')]")
    #
    READY_FOR_RELOCATION_INPUT = (By.ID, "checkbox_readyForRelocation")  # не кликабельный, содержит атрибут checked
    READY_FOR_RELOCATION_CHECKBOX = (
    By.XPATH, "//input[@id='checkbox_readyForRelocation']/..")  # кликабельная обертка чекбокса
    RELOCATION_CITIES = (By.NAME, "relocationCities")  # не кликабельный
    # Работа (Офис, Удаленно)
    WORK_TYPE_OFFICE = (By.XPATH, "//li[@value='office']")
    WORK_TYPE_REMOTE = (By.XPATH, "//li[@value='remote']")
    #
    SPECIALIZATION_ITEM = (By.XPATH, "//input[@name='specializations']/following-sibling::div//li")
    # Опыт управления разработкой
    HAS_MANAGEMENT_EXPERIENCE_INPUT = (
    By.ID, "checkbox_hasManagementExperience")  # не кликабельный, содержит атрибут checked
    HAS_MANAGEMENT_EXPERIENCE_CHECKBOX = (
        By.XPATH, "//input[@id='checkbox_hasManagementExperience']/..")  # кликабельная обертка чекбокса
    ABOUT_MANAGEMENT_EXPERIENCE_INPUT = (
        By.XPATH, "//*[contains(text(), 'об опыте управления')]/../../textarea")  # ----- bad selector
    MANAGEMENT_EXPERIENCE_PERIOD = (By.NAME, "managementExperienceMonth")
    # skills
    AREA_DROPDOWN = lambda index: (By.NAME, f"stackSkills[{index}].skill_area")
    AREA_VALUE = lambda index: (By.XPATH,
                                f"//button[@name='stackSkills[{index}].skill_area']/following-sibling::ul/li")  # может быть больше одного варианта

    TECHNOLOGY_DROPDOWN = lambda index: (By.NAME, f"stackSkills[{index}].skill_technology")
    TECHNOLOGY_VALUE = lambda index: (By.XPATH,
                                      f"//button[@name='stackSkills[{index}].skill_technology']/following-sibling::ul/li")  # может быть больше одного варианта
    EXPERIENCE_DROPDOWN = lambda index: (By.NAME, f"stackSkills[{index}].experience_total")
    EXPERIENCE_VALUE = lambda index: (
        By.XPATH, f"//button[@name='stackSkills[{index}].experience_total']/following-sibling::ul/li")
    SEARCH_JOB_IN_STACK_INPUT = lambda index: (
        By.ID, f"checkbox_stackSkills[{index}].is_primary")  # не кликабельный, содержит атрибут checked
    SEARCH_JOB_IN_STACK_CHECKBOX = lambda index: (
        By.XPATH, f"//input[@id='checkbox_stackSkills[{index}].is_primary']/..")  # кликабельная обертка чекбокса
    DELETE_SKILL_BTN = lambda index: (
    By.XPATH, f"//button[@name='stackSkills[{index}].skill_area']/../../../..//*[text()='Удалить']/..")
    ADD_TECHNOLOGY_BTN = (By.XPATH, "//div[text()='Добавить технологию']/..")
    SKILL_ROW = (By.XPATH, "//table[contains(@class, 'skill-set-select')]/tbody/tr")  # может быть больше одного объекта
    # Используемые в работе инструменты
    TOOLS_INPUT = (By.XPATH, "//*[@id='cv_edit_tools']//input[1]")
    TOOLS_ELEMENTS = (By.XPATH,
                      "//*[@id='cv_edit_tools']//div[contains(@class, 'multiValue')]")  # может быть больше одного объекта
    TOOLS_ELEMENT = lambda index: (
    By.XPATH, f"//*[@id='cv_edit_tools']//div[contains(@class, 'multiValue')][{index + 1}]")
    TOOLS_DELETE_ELEMENT = lambda index: (By.XPATH,
                                          f"//*[@id='cv_edit_tools']//div[contains(@class, 'multiValue')][{index + 1}]//*[contains(@class, 'multi_value_remove')]")
    # Иностранные языки
    ADD_LANG_BTN = (By.XPATH, "//*[text()='Добавить язык']/..")
    LANGUAGE_DROPDOWN = lambda index: (By.NAME, f"languages[{index}].id")
    LANGUAGE_VALUE = lambda index: (By.XPATH, f"//*[@name='languages[{index}].id']/following-sibling::ul/li")
    LEVEL_DROPDOWN = lambda index: (By.NAME, f"languages[{index}].level")
    LEVEL_VALUE = lambda index: (By.XPATH, f"//*[@name='languages[{index}].level']/following-sibling::ul/li")
    DELETE_LANG_BTN = lambda index: (By.XPATH, f"//*[@name='languages[{index}].id']/../../../div[3]")
    LANG_ROW = (By.XPATH,
                "//*[contains(text(), 'Иностранные языки')]/../../div[contains(@class, 'geecko-grid')]")  # может быть больше одного объекта


class CVEditFormCommunity:
    # Участие в сообществах
    TITLE_CONTAINS_TEXT = "Сообщества"
    GITHUB_INPUT = (By.NAME, "linkGitHub")
    GITLAB_INPUT = (By.NAME, "linkGitLab")
    STACKOVERFLOW_INPUT = (By.NAME, "linkStackOverflow")
    HABR_INPUT = (By.NAME, "linkHabr")
    BUTBUCKET_INPUT = (By.NAME, "linkBitBucket")
    TOSTER_INPUT = (By.NAME, "linkToster")
    # Примеры кода
    CODE_EXAMPLE_ROW = (By.XPATH,
                        "//*[contains(text(), 'Примеры кода')]/../following-sibling::div/div[contains(@class, 'geecko-grid')]")  # ----- bad selector
                        # //*[contains(@class, 'qa-code-example')]
    CODE_EXAMPLE_LINK = lambda index: (By.NAME, f"codeSamples[{index}].link")
    # Используемые языки и технологии
    TECHNOLOGIES_INPUT = lambda index: (By.XPATH,
                                        f"//*[@name='codeSamples[{index}].link']/../../..//input[contains(@id, 'react-select')]")
                                        # //*[contains(@class, 'qa-code-example')][{index}]//input[contains(@id, 'react-select')]
    TECHNOLOGIES_ELEMENTS = lambda index: (By.XPATH,
                                           f"//*[@name='codeSamples[{index}].link']/../../..//*[contains(@class, 'multiValue')]")  # может быть больше одного объекта
                                        # //*[contains(@class, 'qa-code-example')][{index}]//*[contains(@class, 'multiValue')]
    TECHNOLOGIES_ELEMENT = lambda index_row, index_el: (
    By.XPATH, f"//*[contains(@class, 'qa-code-example')][{index_row}]//*[contains(@class, 'multiValue')][{index_el + 1}]")
    TECHNOLOGIES_DELETE_ELEMENT = lambda index_row, index_el: (By.XPATH,
               f"//*[contains(@class, 'qa-code-example')][{index_row}]//*[contains(@class, 'multiValue')][{index_el + 1}]//*[contains(@class, 'multi_value_remove')]")
    # Короткое описание
    CODE_DESCRIPTION = lambda index: (
    By.XPATH, f"//*[@name='codeSamples[{index}].link']/../../../..//textarea")  # ----- bad selector
    # //*[contains(@class, 'qa-code-example')][{index}]//textarea
    DELETE_CODE_EXAMPLE_BTN = lambda index: (
    By.XPATH, f"//*[@name='codeSamples[{index}].link']/../../../..//button")  # ----- bad selector
    # //*[contains(@class, 'qa-code-example')][{index}]//button

    # Добавить пример кода
    ADD_CODE_EXAMPLE_BTN = (By.XPATH, " //*[contains(text(), 'Добавить пример кода')]/..")
    # //*[contains(@class, 'qa-add-code-example-button')]/button


class CVEditFormPreferences:
    TITLE_CONTAINS_TEXT = "Предпочтения"
    PREFERENCE_ITEM = (By.XPATH, "//input[@name='workAreas']/following-sibling::div//li")
    WORK_FEATURE_ITEM = (By.XPATH, "//input[@name='workFeatures']/following-sibling::div//li")
    DETAIL_INPUT = (By.XPATH, "//*[text()='Расскажите подробнее']/../following-sibling::textarea")
    # //*[contains(@class, 'qa-expectations')]/textarea


class CVEditFormExperience:
    TITLE_CONTAINS_TEXT = "Профильный опыт"
    COMPANY_ROW = (By.XPATH, "//div[@class='geecko-cv-form__experience-company-wrapper']")
    # //div[@class='qa-experience-company-container']
    ADD_COMPANY_BTN = (By.XPATH, "//*[text()='Добавить компанию']/..")
    # //*[contains(@class, 'qa-add-company-button')]/button
    DELETE_COMPANY_BTN = lambda index: (By.XPATH, f"//*[@name='experience[{index}].position']/../../../../../..//*[text()='Удалить компанию']/..")
    # //div[@class='qa-experience-company-container'][1]//*[contains(@class, 'qa-delete-company-button')]/button
    POSITION_INPUT = lambda index: (By.NAME, f"experience[{index}].position")
    COMPANY_NAME_INPUT = lambda index: (By.NAME, f"experience[{index}].companyName")
    COMPANY_LINK_INPUT = lambda index: (By.NAME, f"experience[{index}].companyLink")
    # Период работы в компании
    START_MONTH_DROPDOWN = lambda index: (By.NAME, f"experience[{index}].startMonth")
    START_MONTH_VALUE = lambda index: (By.XPATH,
                                       f"//*[@name='experience[{index}].startMonth']/following-sibling::ul/li")  # может быть больше одного объекта
    START_YEAR_INPUT = lambda index: (By.NAME, f"experience[{index}].startYear")

    FINISH_MONTH_DROPDOWN = lambda index: (By.NAME, f"experience[{index}].finishMonth")
    FINISH_MONTH_VALUE = lambda index: (By.XPATH,
                                        f"//*[@name='experience[{index}].finishMonth']/following-sibling::ul/li")  # может быть больше одного объекта
    FINISH_YEAR_INPUT = lambda index: (By.NAME, f"experience[{index}].finishYear")
    # чекбокс по настоящее время
    IS_CONTINIOUS_INPUT = lambda index: (By.ID, f"checkbox_experience[{index}].isContinuous")  # не кликабельный, содержит атрибут checked
    IS_CONTINIOUS_CHECKBOX = lambda index: (By.XPATH, f"//input[@id='checkbox_experience[{index}].isContinuous']/..")  # кликабельная обертка чекбокса
    # Чем занимались
    COMMON_DESCRIPTION_TEXTAREA = lambda index: (By.XPATH, f"//*[@name='experience[{index}].position']/../../../../..//textarea")  # ----- bad selector
    TECHNOLOGIES_INPUT = lambda index: (By.XPATH,
                                        f"//*[@name='experience[{index}].position']/../../../../..//input[contains(@id, 'react-select')]")  # ----- bad selector
    TECHNOLOGIES_ELEMENTS = lambda index: (By.XPATH,
                                           f"//*[@name='experience[{index}].position']/../../../../..//*[contains(@class, 'multiValue')]")  # может быть больше одного объекта
    TECHNOLOGIES_ELEMENT = lambda index_row, index_el: (
        By.XPATH,
        f"//*[@name='experience[{index_row}].position']/../../../../..//*[contains(@class, 'multiValue')][{index_el + 1}]")
    TECHNOLOGIES_DELETE_ELEMENT = lambda index_row, index_el: (By.XPATH,
        f"//*[@name='experience[{index_row}].position']/../../../../..//*[contains(@class, 'multiValue')][{index_el + 1}]//*[contains(@class, 'multi_value_remove')]")


class CVEditFormEducation:
    TITLE_CONTAINS_TEXT = "Образование"
    ADD_INSTITUTION_BTN = (By.XPATH, "//*[contains(@class, 'qa-add-institution-button')]/button")
    # Учебное заведение
    INSTITUTION_INPUT = lambda index: (By.XPATH, f"//*[contains(@class, 'qa-institution-container')][{index+1}]//*[contains(@class, 'qa-institution-input')]//input[contains(@id, 'react-select')]")
    INSTITUTION_VALUE = lambda index: (By.XPATH, f"//*[contains(@class, 'qa-institution-container')][{index+1}]//*[contains(@class, 'qa-institution-input')]//div[contains(@class, 'singleValue')]")
    # Факультет
    FACULTY_INPUT = lambda index: (By.XPATH, f"//*[contains(@class, 'qa-institution-container')][{index+1}]//*[contains(@class, 'qa-faculty-input')]//input[contains(@id, 'react-select')]")
    FACULTY_VALUE = lambda index: (By.XPATH, f"//*[contains(@class, 'qa-institution-container')][{index+1}]//*[contains(@class, 'qa-faculty-input')]//div[contains(@class, 'singleValue')]")
    # Период учебы
    START_MONTH_DROPDOWN = lambda index: (By.NAME, f"education[{index}].startMonth")
    START_MONTH_VALUE = lambda index: (By.XPATH,
                                       f"//*[@name='education[{index}].startMonth']/following-sibling::ul/li")  # может быть больше одного объекта
    START_YEAR_INPUT = lambda index: (By.NAME, f"education[{index}].startYear")

    FINISH_MONTH_DROPDOWN = lambda index: (By.NAME, f"education[{index}].finishMonth")
    FINISH_MONTH_VALUE = lambda index: (By.XPATH,
                                        f"//*[@name='education[{index}].finishMonth']/following-sibling::ul/li")  # может быть больше одного объекта
    FINISH_YEAR_INPUT = lambda index: (By.NAME, f"education[{index}].finishYear")
    # чекбокс по настоящее время
    IS_CONTINIOUS_INPUT = lambda index: (
    By.ID, f"checkbox_education[{index}].isContinuous")  # не кликабельный, содержит атрибут checked
    IS_CONTINIOUS_CHECKBOX = lambda index: (
    By.XPATH, f"//input[@id='checkbox_education[{index}].isContinuous']/..")  # кликабельная обертка чекбокса
    INSTITUTION_ROW = (By.XPATH, "//*[contains(@class, 'qa-institution-container')]")  # может быт несколько элементов
    DELETE_INSTITUTION_BTN = lambda index: (
        By.XPATH,
        f"//*[contains(@class, 'qa-institution-container')][{index + 1}]//*[contains(@class, 'qa-delete-institution-button')]/button")

