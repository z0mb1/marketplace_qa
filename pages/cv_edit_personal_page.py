from pages.base_page import BasePageCVEdit
from .form_elements import Input, BaseElement, DropDown
from .locators import CVEditFormPersonal
import random
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import settings
from selenium.webdriver.common.keys import Keys


class CVEditPersonalPage(BasePageCVEdit):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.avatar = Avatar(browser)
        self.fullname = Input(browser, CVEditFormPersonal.FULLNAME_INPUT, 'text', True)
        self.email = Input(browser, CVEditFormPersonal.EMAIL_INPUT, 'text', True)
        self.phone = Input(browser, CVEditFormPersonal.PHONE_INPUT, 'text', True)
        self.birth = Input(browser, CVEditFormPersonal.BIRTH, 'text', True)  # тип text чтобы можно было вводить дату с 0 "05121992"
        #self.birth = browser.find_element(*CVEditFormPersonal.BIRTH)
        self.add_contact_btn = BaseElement(browser, CVEditFormPersonal.ADD_CONTACT_BTN)
        self.other_contacts = OtherContactsSet(browser)

    def get_values(self):
        return {#'avatar': self.avatar.get_value(), # после сохранения значение в инпуте обнуляется, а название картинки в блоке img хэшируется, поэтому корректное сохранение данных не проверить
                'fullname': self.fullname.get_value(),
                'email': self.email.get_value(),
                'phone': self.phone.get_value(),
                'birth': self.birth.get_value(), #.get_attribute('value'),
                'other_contacts': self.other_contacts.get_value()}

    def clear(self):
        self.avatar.clear()
        self.fullname.clear_value()
        #self.email.clear_value()
        self.phone.clear_value()
        self.birth.clear_value() # временно отключено изменение этого опля, потому что некорректно работает очистка этого инпута
        self.other_contacts.clear()

    def clear_optional_fields(self):
        self.avatar.clear()
        self.other_contacts.clear()

    def fill_values(self):
        """поле email нельзя редактировать
           поле birth заполняется нестабильно, часто возникают ошибки в selenium
           поле avatar не заполняем, оно не проверяется
        """
        #self.clear()
        #self.avatar.fill_input()
        self.fullname.fill_input()
        #self.email.fill_input("test@test.com")
        self.phone.fill_input(settings.random_phone())
        #if self.birth.get_attribute('value') == '__.__.____':
        #if self.birth.get_value() == '__.__.____':
            #self.birth.send_keys(Keys.ENTER)  # без этой строчки не будет заполняться,
            #self.birth.send_keys('05121992')
        self.birth.fill_input('05121992')
        time.sleep(1)
        #self.birth.fill_input('05121992')
        self.other_contacts.add_other_contact()
        self.other_contacts.add_other_contact()
        time.sleep(1)
        self.other_contacts.fill_values()

    def make_other_contact(self):
        self.other_contacts.add_other_contact()
        self.other_contacts.add_other_contact()
        self.other_contacts.fill_values()


class Avatar:
    def __init__(self, browser):
        self.browser = browser
        self.input = Input(browser, CVEditFormPersonal.AVATAR_INPUT, 'file')

    def is_avatar_present(self):
        if self.input.is_element_present(*CVEditFormPersonal.AVATAR_IMAGE):
            return True
        return False

    def clear(self):
        if self.is_avatar_present():
            self.delete_avatar()
            WebDriverWait(self.browser, 5).until_not(
                EC.presence_of_element_located(CVEditFormPersonal.AVATAR_IMAGE)
            )

    def fill_input(self):
        self.clear()
        self.input.fill_input(value='avatar.jpg')
        try:
            WebDriverWait(self.browser, 5).until(
                EC.presence_of_element_located(CVEditFormPersonal.AVATAR_IMAGE)
            )
        except:
            pass

    def should_avatar_present(self):
        assert self.is_avatar_present(), 'avatar is not present in page'

    def delete_avatar(self):
        delete_btn = WebDriverWait(self.browser, 5).until(
            EC.element_to_be_clickable(CVEditFormPersonal.AVATAR_REMOVE)
        )
        delete_btn.click()
        try:
            WebDriverWait(self.browser, 5).until_not(
                EC.presence_of_element_located(CVEditFormPersonal.AVATAR_IMAGE)
            )
        except:
            pass

    def get_value(self):
        return self.input.get_value()


class OtherContact:
    def __init__(self, browser, index):
        self.browser = browser
        self.index = index
        self.contact_type = DropDown(browser, CVEditFormPersonal.CONTACT_TYPE(index),
                                     CVEditFormPersonal.CONTACT_TYPE_ELEMENT(index),
                                     index, True)
        self.contact_value = Input(browser, CVEditFormPersonal.CONTACT_VALUE(index), 'text')
        self.delete_btn = self.browser.find_element(*CVEditFormPersonal.DELETE_CONTACT_BTN(index))

    def make_contact_set(self, type_index):
        self.contact_type.choose_value(type_index)
        self.contact_value.fill_input()

    def delete(self):
        self.delete_btn.click()
        try:
            confirm = self.browser.switch_to.alert
            confirm.accept()
            WebDriverWait(self.browser, 5).until_not(
                EC.presence_of_element_located(CVEditFormPersonal.DELETE_CONTACT_BTN(self.index))
            )
        except:
            pass


    def get_value(self):
        return {self.contact_type.get_value(): self.contact_value.get_value()}



class OtherContactsSet:
    def __init__(self, browser):
        self.browser = browser
        self.other_contacts = self.get_other_contacts()
        self.add_contact_btn = BaseElement(browser, CVEditFormPersonal.ADD_CONTACT_BTN)
        self.count = self.get_number_of_rows()

    def add_other_contact(self):
        self.add_contact_btn.click()
        time.sleep(1)
        self.other_contacts.append(OtherContact(self.browser, self.count))
        self.count += 1

    def delete_other_contact(self, index=None):
        """удаляет контакт со страницы, уменьшает счетчик контактов, удаляет значение из массива контактов"""
        if not index:
            index = self.get_number_of_rows() - 1
        print(index, self.get_value())
        contact = self.other_contacts[index]
        contact.delete()
        self.other_contacts.pop()  # при удалении любого элемента индексы оставшихся сдвигаются, т.е. всегда со страницы удалится элемент с наибольшим индексом
        self.count -= 1
        print('delete contact')

    def get_number_of_rows(self):
        return len(self.browser.find_elements(*CVEditFormPersonal.CONTACT_ROW))

    def clear(self):
        for _ in range(self.get_number_of_rows()):
            self.delete_other_contact()

    def fill_values(self):
        for i, contact in enumerate(self.other_contacts):
            contact.make_contact_set(i)  # индкс нужен, потму что одинаковые значения выбрать нельзя, а без указания индекса будут выбираться случайные значения

    def get_value(self):
        res = {}
        for contact in self.other_contacts:
            res.update(contact.get_value())
        return res

    def get_other_contacts(self):
        return [OtherContact(self.browser, n) for n in range(self.get_number_of_rows())]

    def should_be_correct_number_of_rows(self):
        assert self.count == self.get_number_of_rows(), f"number of other contact rows {self.count} is not correct {self.get_number_of_rows()}"
