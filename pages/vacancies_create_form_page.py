from .base_page import BasePage
from .locators import VacanciesCreateForm
import random
import settings
import time
from selenium.common.exceptions import NoSuchElementException
from .form_elements import *


class VacanciesCreateFormPage(BasePage):

    def __init__(self, browser, url):
        super().__init__(browser, url)
        # поля формы
        self.name = Input(browser, VacanciesCreateForm.NAME_INPUT)
        self.quantity = Input(browser, VacanciesCreateForm.QUANTITY_INPUT)
        self.currency_tab = Tab(browser, VacanciesCreateForm.CURRENCY_RUB, VacanciesCreateForm.CURRENCY_USD)
        self.period_tab = Tab(browser, VacanciesCreateForm.PERIOD_MONTH, VacanciesCreateForm.PERIOD_YEAR)
        self.salary_amount = SalaryAmount(
            browser, VacanciesCreateForm.SALARY_FROM_INPUT, VacanciesCreateForm.SALARY_TO_INPUT)
        self.show_salary = CheckBox(browser, VacanciesCreateForm.SALARY_IS_VISIBLE_INPUT, VacanciesCreateForm.SALARY_IS_VISIBLE_CHECKBOX)
        self.office_work = OfficeWork(browser)
        self.remote_work = RemoteWork(browser)
        self.is_skills_required = CheckBox(
            browser, VacanciesCreateForm.IS_SKILLS_REQUIRED_INPUT, VacanciesCreateForm.IS_SKILLS_REQUIRED_CHECKBOX)
        self.seniority_levels = SelectGroup(
            browser, VacanciesCreateForm.SENIORITY_LEVEL, settings.SELECTED_MAX_SENIORITY_LEVELS)
        self.specializations = SelectGroup(
            browser, VacanciesCreateForm.SPECIALIZATION_ITEM, settings.SELECTED_MAX_SPECIALIZATIONS)
        self.skill_set = SkillSet(browser)
        self.languages_required = LanguageSet(
            browser, VacanciesCreateForm.LANGUAGES_REQUIRED_INPUT, VacanciesCreateForm.LANGUAGES_REQUIRED_CHECKBOX)
        self.about_company = Input(browser, VacanciesCreateForm.ABOUT_COMPANY_TEXTAREA)
        self.tasks = Input(browser, VacanciesCreateForm.TASKS_TEXTAREA)
        #self.technologies_and_services
        #self.other_skills
        #self.conditions
        #self.options_and_bonuses
        #self.team
        self.submit_btn = self.browser.find_element(*VacanciesCreateForm.SUBMIT_VACANCY_BTN)

    def click_element(self, selector):
        element = self.browser.find_element(*selector)
        element.click()

    def submit_form(self):
        self.submit_btn.click()

    def fill_all_requireq_fields(self):
        self.name.fill_input('text')
        self.quantity.fill_input('number')
        self.salary_amount.salary_from_input.fill_input('number', 200000)
        self.salary_amount.salary_to_input.fill_input('number', 300000)
        self.remote_work.click()
        self.seniority_levels.choose_element()
        self.seniority_levels.choose_element()
        self.specializations.choose_element()
        self.skill_set.skills[0].make_skillset()
        self.skill_set.add_skill()
        self.skill_set.skills[1].make_skillset()
        self.about_company.fill_input('text')
        self.tasks.fill_input('text')

    def get_vacancy_data(self):
        vacancy_data = {'name': self.name.get_value(), 'salary': {'show_salary': self.show_salary.checked,
                                                                  'salary_from': ''.join(self.salary_amount.salary_from_input.get_value().split()),
                                                                  'salary_to': ''.join(self.salary_amount.salary_to_input.get_value().split()),
                                                                  'currency': self.currency_tab.value,
                                                                  'period': self.period_tab.value},
                        'work_type': {'office': self.office_work.get_status(), 'remote': self.remote_work.get_status()},
                        'is_skills_required': self.is_skills_required.is_checked(),
                        'seniority': self.seniority_levels.get_values(), 'skills': self.skill_set.get_skills_values(),
                        'about_company': self.about_company.get_value(), 'tasks': self.tasks.get_value()}

        return vacancy_data




class RemoteWork(ExtraFieldsElement):
    def __init__(self, browser):
        super().__init__(browser, VacanciesCreateForm.WORK_TYPE_REMOTE,
                         VacanciesCreateForm.TIMEZONE_FIELDSET)


class OfficeWork(ExtraFieldsElement):
    def __init__(self, browser):
        super().__init__(browser, VacanciesCreateForm.WORK_TYPE_OFFICE,
                         VacanciesCreateForm.HAS_RELOCATION_SUPPORT_CHECKBOX,
                         VacanciesCreateForm.OFFICE_ADDRESS_DROPDOWN)

class Team(ExtraFieldsElement):
    def __init__(self, browser):
        super().__init__(browser, VacanciesCreateForm.WORK_TYPE_OFFICE,
                         VacanciesCreateForm.HAS_RELOCATION_SUPPORT_CHECKBOX,
                         VacanciesCreateForm.OFFICE_ADDRESS_DROPDOWN)


class SalaryAmount:
    def __init__(self, browser, salary_from_selector, salary_to_selector):
        self.salary_from_input = Input(browser, salary_from_selector)
        self.salary_to_input = Input(browser, salary_to_selector)

    def should_show_error_if_salary_from_greater_than_salary_to(self):
        if int(self.salary_from_input.get_value()) > int(self.salary_to_input.get_value()):
            assert self.salary_to_input.is_element_present(*VacanciesCreateForm.SALARY_RATIO_ERROR), 'salary from can not be greater than salary to'
        else:
            assert not self.salary_to_input.is_element_present(
                *VacanciesCreateForm.SALARY_RATIO_ERROR), 'should not show error message with correct salary ratio'


class LanguageSet(CheckBox):
    def __init__(self, browser, input_selector, clickable_element_selector):
        super().__init__(browser, input_selector, clickable_element_selector)

    def is_languages_set_container_enable(self):
        if self.browser.find_elements(*VacanciesCreateForm.LANGUAGES_SET_CONTAINER):
            return True
        return False

    def should_be_language_set_container(self):
        if self.checked:
            assert self.is_languages_set_container_enable(), 'language set container is not enable with true checkbox'

    def should_not_be_language_set_container(self):
        if not self.checked:
            assert not self.is_languages_set_container_enable(), 'language set container is enable with false checkbox '

    def should_level_dropdown_disable(self):
        pass

    def choose_language(self):
        pass

    def choose_level(self):
        pass

class Skill(BaseElement):
    def __init__(self, browser, index):
        super().__init__(browser)
        self.index = index
        self.area = DropDown(browser, VacanciesCreateForm.AREA_DROPDOWN, VacanciesCreateForm.AREA_VALUE, index)
        self.technology = DropDown(self.browser, VacanciesCreateForm.TECHNOLOGY_DROPDOWN, VacanciesCreateForm.TECHNOLOGY_VALUE, self.index)  # если не выбрана area, то elements еще не будут инициализированы
        self.experience = Input(browser, VacanciesCreateForm.EXPERIENCE_INPUT(index))
        #self.choose_skill_btn = self.browser.find_elements(*VacanciesCreateForm.CHOOSE_SKILL_CHECKBOX)[index]

    def make_skillset(self):
        self.area.choose_random_value()
        self.technology = DropDown(self.browser, VacanciesCreateForm.TECHNOLOGY_DROPDOWN, VacanciesCreateForm.TECHNOLOGY_VALUE, self.index)
        self.technology.choose_random_value()
        self.experience.fill_input('number', random.randint(1, 10))

    def delete_skill(self):
        delete_btn = self.browser.find_elements(*VacanciesCreateForm.DELETE_SKILL_BTN)[self.index]
        delete_btn.click()

    #def choose_skill(self):
    #    self.choose_skill_btn.click()


class SkillSet(BaseElement):
    def __init__(self, browser):
        super().__init__(browser)
        self.skills = self.get_skills()
        self.add_skill_btn = self.browser.find_element(*VacanciesCreateForm.ADD_TECHNOLOGY_BTN)
        self.count = self.get_number_of_skill_rows()

    def add_skill(self):
        self.add_skill_btn.click()
        self.skills.append(Skill(self.browser, len(self.skills)))
        self.count += 1

    def delete_skill(self, index):
        skill = self.skills[index]
        self.skills.remove(skill)
        skill.delete_skill()
        self.count -= 1

    def get_number_of_skill_rows(self):
        return len(self.browser.find_elements(*VacanciesCreateForm.SKILL_ROW))

    def get_skills(self):
        skills = []
        for n in range(self.get_number_of_skill_rows()):
            skills.append(Skill(self.browser, n))
        return skills

    def should_be_correct_number_of_skill_rows(self):
        assert self.count == self.get_number_of_skill_rows(), 'number of skill rows is not correct'

    def get_skills_values(self):
        res = []
        for skill in self.skills:
            res.append({'area': skill.area.get_value(), 'technology': skill.technology.get_value(), 'experience': skill.experience.get_value()})
        return res


## delete this classes

class BaseElement:

    def __init__(self, browser):
        self.browser = browser

    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True


class ExtraFieldsElement(BaseElement):

    def __init__(self, browser, switch_btn_selector, *extra_field_selectors):
        super().__init__(browser)
        self.switch_btn = self.browser.find_element(*switch_btn_selector)
        self.status = self.get_status()
        self.extra_field_selectors = extra_field_selectors

    def get_status(self):
        if self.switch_btn.get_attribute('aria-selected') == 'false':
            return False
        elif self.switch_btn.get_attribute('aria-selected') == 'true':
            return True

    def click(self):
        self.switch_btn.click()
        self.status = not self.status

    def is_status_correct(self):
        assert self.status == self.get_status(), 'status is not correct'

    def should_show_extra_fields(self):
        for extra_field_selector in self.extra_field_selectors:
            if self.status:
                assert self.is_element_present(*extra_field_selector), 'extra field is not present'
            else:
                assert not self.is_element_present(*extra_field_selector), 'extra field is present'


class Input(BaseElement):
    def __init__(self, browser, input_selector):
        super().__init__(browser)
        self.input = self.browser.find_element(*input_selector)

    def fill_input(self, value_type, value=None):
        self.input.clear()
        if value_type == 'text':
            self.input.send_keys(value if value else settings.random_string())
        elif value_type == 'number':
            self.input.send_keys(value if value else random.randint(1, 100))

    def get_value(self):
        return self.input.get_attribute('value')


class Tab:
    def __init__(self, browser, left_element_selector, right_element_selector):
        self.browser = browser
        self.left = self.browser.find_element(*left_element_selector)
        self.right = self.browser.find_element(*right_element_selector)
        self.selected = self.get_selected()
        self.value = self.selected.text

    def get_selected(self):
        if self.left.get_attribute('class').endswith('active'):
            return self.left
        elif self.right.get_attribute('class').endswith('active'):
            return self.right

    def click_unselected_value(self):
        if self.selected == self.left:
            self.right.click()
            self.selected = self.right
        elif self.selected == self.right:
            self.left.click()
            self.selected = self.left
        #time.sleep(3)

    def should_be_correct_selected_value(self):
        # сравнивает значение, хранящееся в атрибуте объекта и изменяющееся по клику с реальным значением атрибута элемента на странице
        # print(self.checked, self.is_checked())
        assert self.selected == self.get_selected(), 'wrong checked value'


class CheckBox:

    def __init__(self, browser, input_selector, clickable_element_selector):
        self.browser = browser
        self.input = self.browser.find_element(*input_selector)
        self.checkbox = self.browser.find_element(*clickable_element_selector)
        self.checked = True if self.input.get_attribute("checked") == 'true' else False

    def click(self):
        self.checkbox.click()
        self.checked = not self.checked
        #time.sleep(3)

    def should_be_correct_checked_value(self):
        # сравнивает значение, хранящееся в атрибуте объекта и изменяющееся по клику с реальным значением атрибута элемента на странице
        #print(self.checked, self.is_checked())
        assert self.checked == self.is_checked(), 'wrong checkbox value'

    def is_checked(self):
        if self.input.get_attribute("checked") == 'true':
            return True
        return False


class SelectGroup:
    def __init__(self, browser, elements_selector, max_elements):
        self.elements = browser.find_elements(*elements_selector)
        self.number_of_elements = len(self.elements)
        self.clicked_elements = []
        self.max_elements = max_elements

    def get_values(self):
        res = []
        for i in self.clicked_elements:
            res.append(self.elements[i].text)
        return res

    def is_element_selected(self, element):
        selected = element.get_attribute("aria-selected")
        if selected == 'true':
            return True
        return False

    def click_element(self, index_of_element):
        self.elements[index_of_element].click()
        #time.sleep(3)

    def choose_element(self, random_choice=True, i=None):
        if len(self.clicked_elements) >= self.number_of_elements:
            raise AssertionError("All elements are already chosen")
        if random_choice:
            i = random.choice(list(set(range(self.number_of_elements)) - set(self.clicked_elements)))
        #print(f'clicked to index{i}')
        self.click_element(i)
        # если уровень был выбран ранее, проверяем, что после клика элемент не выбран
        if i in self.clicked_elements:
            self.clicked_elements.remove(i)
            assert not self.is_element_selected(
                self.elements[i]), 'element is still selected after canceled'
        # если элемент не был выбран ранее
        else:
            #print(self.clicked_elements)
            # если превышен лимит, то проверяем, что новый элемент не выбирается
            if len(self.clicked_elements) >= self.max_elements:
                assert not self.is_element_selected(self.elements[i]), 'element selected over max value'
            # добавляем его в список выбраных и проверяем, что элемент выбран
            else:
                self.clicked_elements.append(i)
                assert self.is_element_selected(self.elements[i]), 'element is not selected'


class DropDown(BaseElement):
    def __init__(self, browser, dropdown_selector, element_selector, index):
        super().__init__(browser)
        self.dropdown = self.browser.find_element(*dropdown_selector(index))
        self.elements = self.browser.find_elements(*element_selector(index))
        self.value = self.get_value()

    def choose_random_value(self):
        self.dropdown.click()
        element = random.choice(self.elements)
        element.click()
        self.value = self.get_value()

    def get_value(self):
        if 'Выберите' in self.dropdown.get_attribute('title'):
            return None
        return self.dropdown.get_attribute('title')