import settings
import urls
import time
import pytest
from pages.cv_edit_preferences_page import CVEditPreferencesPage
from pages.locators import CVEditFormPreferences, Jobs
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


cv_edit_preferences_url = settings.DOMAIN + urls.CV_EDIT_PREFERENCES
cd_edit_experience_url = settings.DOMAIN + urls.CV_EDIT_EXPERIENCE


@pytest.fixture(scope="function")
def cv_edit_preferences_page(browser, make_login_as_candidate):
    WebDriverWait(browser, 10).until(
        EC.title_contains(Jobs.TITLE_CONTAINS_TEXT)
    )
    browser.get(cv_edit_preferences_url)
    cv_edit_preferences_page = CVEditPreferencesPage(browser, cv_edit_preferences_url)
    WebDriverWait(browser, 10).until(
        EC.title_contains(CVEditFormPreferences.TITLE_CONTAINS_TEXT)
    )
    return cv_edit_preferences_page

'''
#@pytest.mark.skip
def test_can_not_choose_more_than_max_preferences(cv_edit_preferences_page):
    cv_edit_preferences_page.preferences.clear_elements()
    for _ in range(settings.SELECTED_MAX_PREFERENCES + 1):
        cv_edit_preferences_page.preferences.choose_element()

#@pytest.mark.skip
def test_can_can_not_choose_more_than_max_work_features(cv_edit_preferences_page):
    cv_edit_preferences_page.work_features.clear_elements()
    for _ in range(settings.SELECTED_MAX_WORK_FEATURES + 1):
        cv_edit_preferences_page.work_features.choose_element()
'''


#@pytest.mark.skip
# в некоторых запусках была ошибка ElementClickInterceptedException при клике на 19, 14 preferences - разобраться
def test_save_data_correct(browser, cv_edit_preferences_page):
    cv_edit_preferences_page.fill_values()
    data = cv_edit_preferences_page.get_values()
    print(data)
    cv_edit_preferences_page.save()
    cv_edit_preferences_page.refresh()
    time.sleep(3)
    page_after_refresh = CVEditPreferencesPage(browser, cv_edit_preferences_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "data saves incorrect"

#@pytest.mark.skip
def test_save_blank_data_correct(browser, cv_edit_preferences_page):
    cv_edit_preferences_page.clear()
    data = cv_edit_preferences_page.get_values()
    print(data)
    cv_edit_preferences_page.save()
    cv_edit_preferences_page.refresh()
    time.sleep(3)
    page_after_refresh = CVEditPreferencesPage(browser, cv_edit_preferences_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert {'preferences': [], 'work_features': [], 'detail': ''} == saved_data, "blank data saves incorrect"

