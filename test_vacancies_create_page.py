from pages.vacancies_create_form_page import VacanciesCreateFormPage
from pages.vacancies_page import VacanciesPage
from pages.vacancy_page import VacancyPage
from pages.vacancies_create_choose_tariff_page import VacanciesCreateChooseTariffPage
import pytest
import settings
import urls
from pages.locators import VacanciesPageLocators, VacanciesCreateChooseTariffPageLocators, Vacancy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


vacancies_url = settings.DOMAIN + urls.VACANCIES
vacancies_create_url = settings.DOMAIN + urls.VACANCIES_CREATE_CHOOSE_TARIFF
vacancies_create_form_pro = settings.DOMAIN + urls.VACANCIES_CREATE_TARIFF_PRO


@pytest.fixture(scope="module")
def vacancy_create_form_page(browser, make_login):
    WebDriverWait(browser, 10).until(
        EC.title_contains(VacanciesPageLocators.TITLE_CONTAINS_TEXT)
    )
    vacancies_page = VacanciesPage(browser, browser.current_url)
    vacancies_page.should_be_url(vacancies_url)
    vacancies_page.click_create_vacancies_btn()
    WebDriverWait(browser, 10).until(
        EC.title_contains(VacanciesCreateChooseTariffPageLocators.TITLE_CONTAINS_TEXT)
    )
    vacancies_create_choose_tariff_page = VacanciesCreateChooseTariffPage(browser, browser.current_url)
    vacancies_create_choose_tariff_page.should_be_url(settings.DOMAIN + urls.VACANCIES_CREATE_CHOOSE_TARIFF)
    if vacancies_create_choose_tariff_page.is_element_present(*VacanciesCreateChooseTariffPageLocators.WRAPPER):
        vacancies_create_choose_tariff_page.close_wrapper()
    vacancies_create_choose_tariff_page.click_choose_tariff_btn('Про-подбор')
    page = VacanciesCreateFormPage(browser, vacancies_create_form_pro)
    return page


# название вакансии

# количество сотрудников

# зарплата
#@pytest.mark.skip
def test_can_change_currency(vacancy_create_form_page):
    vacancy_create_form_page.currency_tab.should_be_correct_selected_value()
    vacancy_create_form_page.currency_tab.click_unselected_value()
    vacancy_create_form_page.currency_tab.should_be_correct_selected_value()
    vacancy_create_form_page.currency_tab.click_unselected_value()
    vacancy_create_form_page.currency_tab.should_be_correct_selected_value()

#@pytest.mark.skip
def test_can_change_period(vacancy_create_form_page):
    vacancy_create_form_page.period_tab.should_be_correct_selected_value()
    vacancy_create_form_page.period_tab.click_unselected_value()
    vacancy_create_form_page.period_tab.should_be_correct_selected_value()
    vacancy_create_form_page.period_tab.click_unselected_value()
    vacancy_create_form_page.period_tab.should_be_correct_selected_value()

#@pytest.mark.skip
def test_salary_from_should_be_less_or_equal_than_salary_to(vacancy_create_form_page):
    vacancy_create_form_page.salary_amount.salary_to_input.fill_input('number', 1)
    vacancy_create_form_page.salary_amount.salary_from_input.fill_input('number', 2)
    vacancy_create_form_page.salary_amount.should_show_error_if_salary_from_greater_than_salary_to()
    vacancy_create_form_page.salary_amount.salary_to_input.fill_input('number', 3)
    vacancy_create_form_page.salary_amount.should_show_error_if_salary_from_greater_than_salary_to()

#@pytest.mark.skip
def test_show_salary_checkbox_works(vacancy_create_form_page):
    vacancy_create_form_page.show_salary.should_be_correct_checked_value()
    vacancy_create_form_page.show_salary.click()
    vacancy_create_form_page.show_salary.should_be_correct_checked_value()
    vacancy_create_form_page.show_salary.click()

# работа офис
#@pytest.mark.skip
def test_should_show_extra_fields_if_choose_offic_work(vacancy_create_form_page):
    vacancy_create_form_page.office_work.should_show_extra_fields()
    vacancy_create_form_page.office_work.is_status_correct()
    vacancy_create_form_page.office_work.click()
    vacancy_create_form_page.office_work.is_status_correct()
    vacancy_create_form_page.office_work.should_show_extra_fields()
    vacancy_create_form_page.office_work.click()
    vacancy_create_form_page.office_work.is_status_correct()

# работа удаленно
#@pytest.mark.skip
def test_should_show_extra_fields_if_choose_remote_work(vacancy_create_form_page):
    vacancy_create_form_page.remote_work.should_show_extra_fields()
    vacancy_create_form_page.remote_work.is_status_correct()
    vacancy_create_form_page.remote_work.click()
    vacancy_create_form_page.remote_work.is_status_correct()
    vacancy_create_form_page.remote_work.should_show_extra_fields()
    vacancy_create_form_page.remote_work.click()
    vacancy_create_form_page.remote_work.is_status_correct()

# принимаем тестирование Geecko
#@pytest.mark.skip
def test_is_skills_required_checkbox_works(vacancy_create_form_page):
    vacancy_create_form_page.is_skills_required.should_be_correct_checked_value()
    vacancy_create_form_page.is_skills_required.click()
    vacancy_create_form_page.is_skills_required.should_be_correct_checked_value()

# уровень
#@pytest.mark.skip
def test_can_select_and_cancel_seniority_level(vacancy_create_form_page):
    vacancy_create_form_page.seniority_levels.choose_element(random_choice=False, i=0)
    vacancy_create_form_page.seniority_levels.choose_element(random_choice=False, i=0)

#@pytest.mark.skip
def test_can_not_choose_more_than_max_levels(vacancy_create_form_page):
    for _ in range(settings.SELECTED_MAX_SENIORITY_LEVELS + 1):
        vacancy_create_form_page.seniority_levels.choose_element()

# специализация
#@pytest.mark.skip
def test_can_select_and_cancel_specialization(vacancy_create_form_page):
    vacancy_create_form_page.specializations.choose_element(random_choice=False, i=0)
    vacancy_create_form_page.specializations.choose_element(random_choice=False, i=0)

#@pytest.mark.skip
def test_can_not_choose_more_than_max_specializations(vacancy_create_form_page):
    for _ in range(settings.SELECTED_MAX_SPECIALIZATIONS + 1):
        vacancy_create_form_page.specializations.choose_element()


# языки и технологии
#@pytest.mark.skip
def test_can_fill_skillset(vacancy_create_form_page):
    vacancy_create_form_page.skill_set.skills[0].make_skillset()

#@pytest.mark.skip
def test_can_add_skillset(vacancy_create_form_page):
    vacancy_create_form_page.skill_set.should_be_correct_number_of_skill_rows()
    vacancy_create_form_page.skill_set.add_skill()
    vacancy_create_form_page.skill_set.should_be_correct_number_of_skill_rows()
    vacancy_create_form_page.skill_set.delete_skill(1)
    vacancy_create_form_page.skill_set.should_be_correct_number_of_skill_rows()


# иностранные языки
#@pytest.mark.skip
def test_languages_required_checkbox_works(vacancy_create_form_page):
    vacancy_create_form_page.languages_required.should_be_correct_checked_value()
    vacancy_create_form_page.languages_required.click()
    vacancy_create_form_page.languages_required.should_be_correct_checked_value()
    vacancy_create_form_page.languages_required.click()

#@pytest.mark.skip
def test_language_set_swithch_works(vacancy_create_form_page):
    vacancy_create_form_page.languages_required.should_not_be_language_set_container()
    vacancy_create_form_page.languages_required.click()
    vacancy_create_form_page.languages_required.should_be_language_set_container()
    vacancy_create_form_page.languages_required.click()

@pytest.mark.skip
def test_choose_language_works(vacancy_create_form_page):
    pass


@pytest.mark.skip
def test_add_language_works():
    pass

@pytest.mark.skip
def test_remove_language_works():
    pass
# команда
# ответственный

@pytest.mark.skip
def test_can_not_create_vacancy_with_one_blank_required_field():
    pass

#@pytest.mark.skip
def test_can_create_vacancy_with_only_required_fields(browser, vacancy_create_form_page):
    vacancy_create_form_page.fill_all_requireq_fields()
    vacancy_form_data = vacancy_create_form_page.get_vacancy_data()
    print(vacancy_form_data)
    vacancy_create_form_page.submit_form()
    WebDriverWait(browser, 20).until(
        EC.title_contains(Vacancy.TITLE_CONTAINS_TEXT)
    )
    assert browser.current_url.split('/')[-2] == 'vacancy', "can not create new vacancy"
    vacancy = VacancyPage(browser, browser.current_url)
    vacancy_page_data = vacancy.vacancy_data
    print(vacancy_page_data)

    assert vacancy_form_data['name'] == vacancy_page_data['name'], 'not equal name'
    assert vacancy_form_data['salary']['show_salary'] == vacancy_page_data['salary']['show_salary'], 'not equal show salary'
    if vacancy_form_data['salary']['show_salary']:
        assert vacancy_form_data['salary']['salary_from'] == vacancy_page_data['salary']['salary_from'], 'not equal salary_from'
        assert vacancy_form_data['salary']['salary_to'] == vacancy_page_data['salary']['salary_to'], 'not equal salary_to'
        assert vacancy_form_data['salary']['currency'] == vacancy_page_data['salary']['currency'], 'not equal currency'
    assert vacancy_form_data['work_type']['office'] == vacancy_page_data['work_type']['office'], 'not equal work type office'
    assert vacancy_form_data['work_type']['remote'] == vacancy_page_data['work_type']['remote'], 'not equal work type remote'
    #assert vacancy_form_data['is_skills_required'] == vacancy_page_data['is_skills_required'], 'not equal is_skills_required' офидается фикс
    assert set(vacancy_form_data['seniority']) == set(vacancy_page_data['seniority']), 'not equal seniority'
    for form_data, page_data in zip(vacancy_form_data['skills'], vacancy_page_data['skills']):
        assert form_data['technology'] == page_data['technology'], 'not equal skill area'
        assert form_data['experience'] == page_data['experience'], 'not equal skill area'
    assert vacancy_form_data['tasks'] == vacancy_page_data['tasks'], 'not equal tasks'
