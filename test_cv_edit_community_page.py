import settings
import urls
import time
import pytest
from pages.cv_edit_community_page import CVEditCommunityPage
from pages.locators import Jobs, CVEditFormCommunity
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


cv_edit_community_url = settings.DOMAIN + urls.CV_EDIT_COMMUNITY
cv_edit_preferences_url = settings.DOMAIN + urls.CV_EDIT_PREFERENCES


@pytest.fixture(scope="function")
def cv_edit_community_page(browser, make_login_as_candidate):
    WebDriverWait(browser, 10).until(
        EC.title_contains(Jobs.TITLE_CONTAINS_TEXT)
    )
    browser.get(cv_edit_community_url)
    WebDriverWait(browser, 10).until(
        EC.title_contains(CVEditFormCommunity.TITLE_CONTAINS_TEXT)
    )
    cv_edit_community_page = CVEditCommunityPage(browser, cv_edit_community_url)
    return cv_edit_community_page


#@pytest.mark.skip
def test_save_data_correct(browser, cv_edit_community_page):
    cv_edit_community_page.clear()
    cv_edit_community_page.fill_community_inputs()
    data = cv_edit_community_page.get_values()
    print(data)
    cv_edit_community_page.save()
    cv_edit_community_page.refresh()
    time.sleep(3)
    page_after_refresh = CVEditCommunityPage(browser, cv_edit_community_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "data saves incorrect"

#@pytest.mark.skip
def test_save_blank_data_correct(browser, cv_edit_community_page):
    cv_edit_community_page.clear()
    data = cv_edit_community_page.get_values()
    print(data)
    cv_edit_community_page.save()
    cv_edit_community_page.refresh()
    time.sleep(3)
    page_after_refresh = CVEditCommunityPage(browser, cv_edit_community_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert {'github': 'https://github.com/',
            'gitlab': 'https://gitlab.com/',
            'stackoverflow': 'https://stackoverflow.com/users/',
            'habr': 'https://habr.com/users/',
            'bitbucket': 'https://bitbucket.org/',
            'toster': 'https://toster.ru/user/',
            'code_examples': [{'code_example_link': '', 'technologies': set(), 'code_description': ''}]} == saved_data, "blank data saves incorrect"
