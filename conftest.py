import pytest
from selenium import webdriver
import settings
import urls
from pages.login_page import LoginPage
import time

def pytest_addoption(parser):
    parser.addoption('--browser_name', action='store', default='chrome',
                     help="Choose browser: chrome or firefox")


@pytest.fixture(scope="function")
def browser(request):
    browser_name = request.config.getoption("browser_name")
    browser = None
    if browser_name == "chrome":
        # print("\nstart chrome browser for test..")
        browser = webdriver.Chrome()
    elif browser_name == "firefox":
        # print("\nstart firefox browser for test..")
        browser = webdriver.Firefox()
    else:
        raise pytest.UsageError("--browser_name should be chrome or firefox")
    yield browser
    # print("\nquit browser..")
    #time.sleep(10)
    browser.quit()

@pytest.fixture(scope="module")
def make_login(request, browser):
    login_url = settings.DOMAIN + urls.LOGIN
    page = LoginPage(browser, login_url)
    page.open()
    page.should_be_email_and_password_inputs()
    page.login(settings.ACC_EMAIL, settings.ACC_PASSWORD)

@pytest.fixture(scope="function")
def make_login_as_candidate(request, browser):
    login_url = settings.DOMAIN + urls.LOGIN
    page = LoginPage(browser, login_url)
    page.open()
    page.should_be_email_and_password_inputs()
    page.login(settings.CANDIDATE_EMAIL, settings.CANDIDATE_PASSWORD)