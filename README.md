автотесты для marketplace:
http://marketplace_sprint.iwu.9ev.ru/  
Описание:  
https://www.notion.so/iwuteam/bd3f1c475fa44f9fae167a832ef42841

Для каждой страница проверяется корректность сохранения данных в двух вариантах:
- при заполнении всех полей (более точный список в notion)
- при заполнении только обязательных полей

Тестируемые страницы:
1. Резюме: Персональная информация
2. Резюме: Основная информация
3. Резюме: Сообществам
4. Резюме: Предпочтения
5. Резюме: Профильный опыт  
-----в процессе разработки-----    
6. Резюме: Образование   
-----требует обновления-----    
7. Вакансия

Подготовка среды
```
pip install -r requirements.txt  
sudo dpkg -i google-chrome-stable_current_amd64.deb  
unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/local/bin/chromedriver  
sudo chown root:root /usr/local/bin/chromedriver  
sudo chmod +x /usr/local/bin/chromedriver  
```

Команда для запуска тестов редактирования резюме (с 3 перезапусками в случае неуспешного выполнения)  
Команда запускает 10 тестов (по 2 на каждую страницу), без перезапусков тесты выполняются около 10 минут
```
 pytest test_cv_edit_common_page.py test_cv_edit_community_page.py test_cv_edit_experience_page.py test_cv_edit_personal_page.py test_cv_edit_preferences_page.py -s -vv --reruns 3 >> log.txt
```
