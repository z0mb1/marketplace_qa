import settings
import urls
import time
import pytest
from pages.cv_edit_common_page import CVEditCommonPage
from pages.locators import CVEditFormCommon, Jobs, CVEditFormCommunity
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


cv_edit_common_url = settings.DOMAIN + urls.CV_EDIT_COMMON
cd_edit_community_url = settings.DOMAIN + urls.CV_EDIT_COMMUNITY


@pytest.fixture(scope="function")
def cv_edit_common_page(browser, make_login_as_candidate):
    WebDriverWait(browser, 10).until(
        EC.title_contains(Jobs.TITLE_CONTAINS_TEXT)
    )
    browser.get(cv_edit_common_url)
    WebDriverWait(browser, 10).until(
        EC.title_contains(CVEditFormCommon.TITLE_CONTAINS_TEXT)
    )
    cv_edit_common_page = CVEditCommonPage(browser, cv_edit_common_url)
    return cv_edit_common_page


#@pytest.mark.skip
def test_save_data_correct(browser, cv_edit_common_page):
    #print(cv_edit_common_page.get_values())
    cv_edit_common_page.fill_inputs()
    data = cv_edit_common_page.get_values()
    print(data)
    cv_edit_common_page.save()
    cv_edit_common_page.refresh()
    page_after_refresh = CVEditCommonPage(browser, cv_edit_common_url)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "data saves incorrect"

#@pytest.mark.skip
def test_save_data_with_blank_optional_fields_correct(browser, cv_edit_common_page):
    cv_edit_common_page.clear_optional_fields()
    data = cv_edit_common_page.get_values()
    print(data)
    cv_edit_common_page.save()
    cv_edit_common_page.refresh()
    page_after_refresh = CVEditCommonPage(browser, cv_edit_common_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "blank data saves incorrect"

"""
#@pytest.mark.skip
def test_can_change_currency(cv_edit_common_page):
    cv_edit_common_page.currency.should_be_correct_selected_value()
    cv_edit_common_page.currency.click_unselected_value()
    cv_edit_common_page.currency.should_be_correct_selected_value()
    cv_edit_common_page.currency.click_unselected_value()
    cv_edit_common_page.currency.should_be_correct_selected_value()

#@pytest.mark.skip
def test_can_change_period(cv_edit_common_page):
    cv_edit_common_page.period.should_be_correct_selected_value()
    cv_edit_common_page.period.click_unselected_value()
    cv_edit_common_page.period.should_be_correct_selected_value()
    cv_edit_common_page.period.click_unselected_value()
    cv_edit_common_page.period.should_be_correct_selected_value()

#@pytest.mark.skip
def test_show_salary_checkbox_can_change_status(cv_edit_common_page):
    cv_edit_common_page.show_salary.should_be_correct_checked_value()
    cv_edit_common_page.show_salary.click()
    cv_edit_common_page.show_salary.should_be_correct_checked_value()
    cv_edit_common_page.show_salary.click()

@pytest.mark.skip
def test_should_show_relocation_cities(cv_edit_common_page):
    cv_edit_common_page.ready_to_relocation.should_show_extra_fields()
    cv_edit_common_page.ready_to_relocation.click()
    cv_edit_common_page.ready_to_relocation.should_show_extra_fields()

# работа офис
@pytest.mark.skip
def test_offic_work_tab_can_change_status(cv_edit_common_page):
    cv_edit_common_page.office_work.is_status_correct()
    cv_edit_common_page.office_work.click()
    cv_edit_common_page.office_work.is_status_correct()
    cv_edit_common_page.office_work.click()
    cv_edit_common_page.office_work.is_status_correct()

# работа удаленно
@pytest.mark.skip
def test_remote_work_tab_can_change_status(cv_edit_common_page):
    cv_edit_common_page.remote_work.is_status_correct()
    cv_edit_common_page.remote_work.click()
    cv_edit_common_page.remote_work.is_status_correct()
    cv_edit_common_page.remote_work.click()
    cv_edit_common_page.remote_work.is_status_correct()

@pytest.mark.skip
def test_can_not_choose_more_than_max_specializations(cv_edit_common_page):
    cv_edit_common_page.specializations.clear_elements()
    for _ in range(settings.SELECTED_MAX_SPECIALIZATIONS + 1):
        cv_edit_common_page.specializations.choose_element()

@pytest.mark.skip
def test_should_show_management_experience_fields(cv_edit_common_page):
    cv_edit_common_page.management_experience.should_show_extra_fields()
    cv_edit_common_page.management_experience.click()
    cv_edit_common_page.management_experience.should_show_extra_fields()

# языки и технологии
@pytest.mark.skip
def test_can_add_and_delete_new_skillset_block(cv_edit_common_page):
    cv_edit_common_page.skill_set.should_be_correct_number_of_skill_rows()
    cv_edit_common_page.skill_set.add_skill()
    cv_edit_common_page.skill_set.should_be_correct_number_of_skill_rows()
    cv_edit_common_page.skill_set.delete_skill()
    cv_edit_common_page.skill_set.should_be_correct_number_of_skill_rows()

@pytest.mark.skip
def test_can_fill_skillset(cv_edit_common_page):
    cv_edit_common_page.skill_set.clear()
    cv_edit_common_page.skill_set.add_skill()
    cv_edit_common_page.skill_set.add_skill()
    cv_edit_common_page.skill_set.fill_values()
    print(cv_edit_common_page.skill_set.get_value())

@pytest.mark.skip
def test_search_job_in_stack_works(cv_edit_common_page):
    cv_edit_common_page.skill_set.skills[0].search_job_in_stack.should_be_correct_checked_value()
    cv_edit_common_page.skill_set.skills[0].search_job_in_stack.click()
    cv_edit_common_page.skill_set.skills[0].search_job_in_stack.should_be_correct_checked_value()

# Используемые в работе инструменты
@pytest.mark.skip
def test_tools(cv_edit_common_page):
    print(cv_edit_common_page.tools.get_number_of_items())
    print(cv_edit_common_page.tools.get_value())
    time.sleep(3)
    cv_edit_common_page.tools.add_element()
    print(cv_edit_common_page.tools.get_number_of_items())
    print(cv_edit_common_page.tools.get_value())
    time.sleep(3)
    cv_edit_common_page.tools.add_element()
    print(cv_edit_common_page.tools.get_number_of_items())
    print(cv_edit_common_page.tools.get_value())
    time.sleep(3)
    cv_edit_common_page.tools.delete_element()
    print(cv_edit_common_page.tools.get_number_of_items())
    print(cv_edit_common_page.tools.get_value())
    time.sleep(3)

# Иностранные языки
@pytest.mark.skip
def test_languages_can_add_and_delete_element(cv_edit_common_page):
    #print(cv_edit_common_page.languages.get_number_of_elements(), cv_edit_common_page.languages.count)
    #print(cv_edit_common_page.languages.get_value())
    cv_edit_common_page.languages.clear()
    cv_edit_common_page.languages.should_be_correct_number_of_elements()
    cv_edit_common_page.languages.add_lang()
    cv_edit_common_page.languages.should_be_correct_number_of_elements()
    cv_edit_common_page.languages.delete_lang()
    cv_edit_common_page.languages.should_be_correct_number_of_elements()
    #print(cv_edit_common_page.languages.get_number_of_elements(), cv_edit_common_page.languages.count)
    #print(cv_edit_common_page.languages.get_value())
    cv_edit_common_page.languages.clear()
"""