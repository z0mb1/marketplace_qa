import settings
import urls
import time
import pytest
from pages.cv_edit_education_page import CVEditEducationPage
from pages.locators import Jobs, CVEditFormEducation
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


cv_edit_education_url = settings.DOMAIN + urls.CV_EDIT_EDUCATION


@pytest.fixture(scope="function")
def cv_edit_education_page(browser, make_login_as_candidate):
    WebDriverWait(browser, 10).until(
        EC.title_contains(Jobs.TITLE_CONTAINS_TEXT)
    )
    browser.get(cv_edit_education_url)
    WebDriverWait(browser, 10).until(
        EC.title_contains(CVEditFormEducation.TITLE_CONTAINS_TEXT)
    )
    cv_edit_education_page = CVEditEducationPage(browser, cv_edit_education_url)
    return cv_edit_education_page

#@pytest.mark.skip
def test_save_data_correct(browser, cv_edit_education_page):
    cv_edit_education_page.clear()
    cv_edit_education_page.fill_inputs()
    data = cv_edit_education_page.get_values()
    print(data)
    cv_edit_education_page.save()
    cv_edit_education_page.refresh()
    time.sleep(3)
    page_after_refresh = CVEditEducationPage(browser, cv_edit_education_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "data saves incorrect"

#@pytest.mark.skip
def test_save_data_with_blank_optional_fields_correct(browser, cv_edit_education_page):
    cv_edit_education_page.clear_optional_fields()
    data = cv_edit_education_page.get_values()
    print(data)
    cv_edit_education_page.save()
    cv_edit_education_page.refresh()
    time.sleep(3)
    page_after_refresh = CVEditEducationPage(browser, cv_edit_education_page)
    saved_data = page_after_refresh.get_values()
    print(saved_data)
    assert data == saved_data, "blank data saves incorrect"
