import random
import os


DOMAIN = 'https://marketplace.iwu.9ev.ru/'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

# valid account settings
ACC_EMAIL = 'evgenij.tng10-2@yandex.ru'
ACC_PASSWORD = '349dn3'

CANDIDATE_EMAIL = 'makenitoy@3mailapp.net'  #
CANDIDATE_PASSWORD = 'makenitoy@3mailapp.net'  #
CANDIDATE_FULL_NAME = "Test Testov"

# create vacancy requirements
SELECTED_MAX_SENIORITY_LEVELS = 2
SELECTED_MAX_SPECIALIZATIONS = 3
# create cv preferences
SELECTED_MAX_PREFERENCES = 5
SELECTED_MAX_WORK_FEATURES = 5



def random_string():
    return str('%032x' % random.getrandbits(128))[:30]


def random_email():
    return random_string()[:10] + '@' + random_string()[:5] + '.com'


def random_phone():
    return f"999{''.join((str(random.randint(1, 9)) for _ in range(7)))}"
