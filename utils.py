def get_phone_starts_with_plus_7(number):
    if number.startswith('+7'):
        phone_number = number
    elif number.startswith('8'):
        phone_number = '+7' + number
    else:
        raise ValueError('phone number should starts with "8" or "+7"')
    return phone_number


def get_phone_starts_with_8(number):
    if number.startswith('+7'):
        phone_number = '8' + number[2:]
    elif number.startswith('8'):
        phone_number = number
    else:
        raise ValueError('phone number should starts with "8" or "+7"')
    return phone_number
